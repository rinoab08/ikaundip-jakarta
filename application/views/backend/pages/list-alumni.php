<div class="row">
    <h5 class="mb-3">Data Master Alumni </h5>
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama Alumni</th>
                        <th scope="col">No. Telp</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1 + $this->uri->segment(3); ?>
                    <?php foreach ($alumni->result() as $key => $value) { ?>
                        <tr>
                            <th scope="row"><?= $i ?></th>
                            <?php $i++; ?>
                            <td><?= $value->nama ?></td>
                            <td><?= $value->noHp ?></td>

                            <td><?= ($value->sent == 1) ? 'Terkirim' : 'Pending'; ?></td>
                        </tr>
                    <?php } ?>


                </tbody>

            </table>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $paginator ?>
            </div>
        </div>
    </div>
</div>