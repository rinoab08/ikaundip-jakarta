<table border="1">
    <tr>
        <th scope="col">NIK</th>
        <th scope="col">Nama Penerima Vaksin</th>
        <th scope="col">Jenis Kelamin</th>
        <th scope="col">Tanggal Lahir</th>
        <th scope="col">No. Telp</th>
        <th scope="col">Alamat</th>
        <th scope="col">Hari</th>
        <th scope="col">jam</th>
        <?php $screening = $this->db->get('screening')->result(); ?>
        <?php foreach ($screening as $key => $value) { ?>
            <th><?= $value->idScreening ?></th>
        <?php } ?>
    </tr>
    <tbody>
        <?php $i = 1 + $this->uri->segment(3); ?>
        <?php foreach ($anggota->result() as $key => $value) { ?>
            <tr>

                <?php $i++; ?>
                <td>'<?= $value->nik ?></td>
                <td><?= $value->nama ?></td>
                <td><?= ($value->jenisKelamin == 1) ? 'L' : 'P'; ?></td>
                <td><?= date('Y-m-d', strtotime($value->tanggalLahir))  ?></td>
                <td>'<?= $value->noHp ?></td>
                <td><?= $value->alamat ?></td>
                <td><?= date('d-m-Y', strtotime($value->tglVaksin)) ?></td>
                <td><?= $value->detailJam ?></td>
                <?php $exp_penyakit = explode(';', $value->riwayatPenyakit); ?>
                <?php foreach ($screening as $key_s => $value_s) { ?>
                    <td><?= (in_array($value_s->idScreening, $exp_penyakit)) ? 'Ya' : '-'; ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </tbody>
</table>
<table>
    <?php $screening = $this->db->get('screening')->result(); ?>
        <?php foreach ($screening as $key => $value) { ?>
        <tr>
            <td><?= $value->idScreening ?></td>
            <td><?= $value->deskripsi ?></td>
        </tr>
            
        <?php } ?>
</table>