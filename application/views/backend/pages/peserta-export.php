<table border="1">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Alumni</th>
            <th scope="col">Tanggal Vaksin</th>
            <th scope="col">Jam</th>
            <th scope="col">No. Telp</th>
            
            <th scope="col">Status</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1 + $this->uri->segment(3); ?>
        <?php foreach ($peserta->result() as $key => $value) { ?>
            <tr>
                <th scope="row"><?= $i ?></th>
                <?php $i++; ?>
                <td><?= $value->nama ?></td>
                <td><?= date('d-m-Y', strtotime($value->tanggal)) ?></td>
                <td>
                    <?php 
                    $jam = '-';
                        if ($value->jam != 0) {
                            $getJam = $this->db->get_where('jam', ['idJam' => $value->jam])->row();
                            $jam = $getJam->detailJam;
                        }
                        echo $jam;
                    ?>
                </td>
                <td><?= $value->no_hp ?></td>
                
                <td><?= ($value->status == 0) ? 'Belum Daftar Ulang' : 'Sudah'; ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>