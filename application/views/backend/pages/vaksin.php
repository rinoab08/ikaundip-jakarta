<div class="row">
    <div class="col-sm-9 mb-3">
        <h5 >Data Peserta Vaksin (<strong><?= $allData->num_rows() ?></strong>)</h5>
    </div>
    <div class="col-sm-3 mb-3">
        <form method="GET" action="<?= site_url('vaksin/search')?>">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Nama Penerima Vaksin" name="nama">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </div>
        </form>
    </div>
    
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col" width="5%">#</th>
                        <th scope="col" width="25%">Nama Penerima Vaksin</th>
                        <th scope="col" width="25%">Nama Alumni</th>
                        <th scope="col" width="15%">No. Telp</th>
                        <th scope="col" width="15%">Tanggal Vaksin</th>
                        
                        <?php if ($this->session->userdata('hakAkses') == 1) { ?>
                            <th scope="col" width="15%">#</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1 + $this->uri->segment(3); ?>
                    <?php foreach ($anggota->result() as $key => $value) { ?>
                        <tr>
                            <td><?= $i ?></td>
                            <?php
                            $i++;
                            $failed = '';
                            if ($value->sent == 1) {
                                $failed = '<a href="' . site_url('vaksin/send/' . $value->id) . '" class="badge badge-warning ml-2">kirim tiket</a>';
                            }
                            if($value->status == 1){
                                $label = 'primary';
                                $label_txt = "Alumni";
                            }else{
                                $label = 'success';
                                $label_txt = "Keluarga";
                            }
                            ?>
                            <td><?= $value->nama ?><br><span class="badge badge-<?= $label?>"><?= $label_txt?></span> <?= $failed ?> </td>
                            <td><?= $value->namaAlumni ?></td>
                            <td><?= $value->noHp ?></td>
                            <td><?= date('d-m-Y', strtotime($value->tglVaksin))  ?><br><span class="badge badge-primary font-sm""><?= $value->detailJam ?></span</td>
                            

                            
                            <?php if ($this->session->userdata('hakAkses') == 1) { ?>
                                <td>
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-detail" data-id="<?= $value->id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-hapus" data-id="<?= $value->id ?>" data-nama="<?= $value->nama ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>


                </tbody>

            </table>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $paginator ?>
            </div>
        </div>
    </div>
</div>