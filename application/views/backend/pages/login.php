<div class="row">
    <div class="col-sm-5 mx-auto">
        <div class="card">
            <div class="card-body">
                <form action="<?= site_url('login/auth') ?>" method="post">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" name="username" value="<?= set_value('username') ?>">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" value="<?= set_value('username') ?>">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-block">Login</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>