<div class="row">
    <div class="col-sm-12">
        <h5 class="mb-3">Data Peserta Vaksin Kedua </h5>
    </div>
    <!-- <div class="col-sm-4 mb-3 text-center">
        <label>Sudah Registrasi Ulang</label>
        <h4><?= $register->num_rows() + $register_upt->num_rows() ?></h4>
    </div>
    <div class="col-sm-4 mb-3 text-center">
        <label>Belum Registrasi Ulang</label>
        <h4><?= $ots->num_rows() ?></h4>
    </div> -->
    <div class="col-sm-4 mb-3 text-center">
        <label>Terkirim</label>
        <?php $terkirim = $this->db->get_where('jateng', ['blast' => 1]); ?>
        <h4><?= $terkirim->num_rows() ?></h4>
    </div>
    <div class="col-sm-4 mb-3 text-center">
        <label>Belum Terkirim</label>
        <?php $Pending = $this->db->get_where('jateng', ['blast' => 0]); ?>
        <h4><?= $Pending->num_rows() ?></h4>
    </div>
    <div class="col-sm-6 mb-3">
        <a href="<?= site_url('peserta/Export') ?>" class="btn btn-success mr-3">Export Peserta</a>
        <a href="javascript:void(0)" class="btn btn-primary btn-add">Tambah Peserta</a>
    </div>
    <div class="col-sm-3">

    </div>
    <div class="col-sm-3 mb-3">
        <form method="GET" action="<?= site_url('peserta/search') ?>">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Nama Penerima Vaksin" name="nama">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tiket</th>
                        <th scope="col">Nama Alumni</th>
                        <th scope="col">No. Telp</th>
                        <th scope="col">Tanggal Vaksin</th>
                        <th scope="col">Jam</th>

                        <th scope="col">Status</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1 + $this->uri->segment(3); ?>
                    <?php foreach ($peserta->result() as $key => $value) { ?>
                        <tr>
                            <th scope="row"><?= $i ?></th>
                            <?php $i++; ?>
                            <td><?= $value->tiket ?></td>
                            <?php
                            $checkData = $this->db->get_where('peserta_v1_fix', ['nama' => $value->nama, 'no_hp' => $value->no_hp]);

                            ?>
                            <td>
                                <?= $value->nama ?>

                                <?php
                                $failed = '';
                                if ($value->blast == 0) {
                                    $failed .= '<a href="' . site_url('peserta/send/' . $value->idVaksinP) . '" class="badge badge-warning ml-2">Kirim Tiket</a>';
                                    $failed .= '<a href="' . site_url('peserta/undangan/' . $value->idVaksinP) . '" class="badge badge-primary ml-2">Undangan</a>';
                                }

                                ?>
                                <?= '<br>' . $failed ?>
                            </td>
                            <td><?= $value->no_hp ?></td>
                            <td><?= date('d-m-Y', strtotime($value->tanggal)) ?></td>
                            <?php $jam = $value->jam;
                            if (!empty($value->jam)) {
                                $getJam = $this->db->get_where('jam', ['idJam' => $value->jam])->row();
                                $jam = $getJam->detailJam;
                            }
                            ?>
                            <td><?= $jam ?></td>
                            <td><?= ($value->blast == 0) ? 'pending' : 'terkirim'; ?></td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-detail-2" data-id="<?= $value->idVaksinP ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </td>

                        </tr>
                    <?php } ?>


                </tbody>

            </table>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $paginator ?>
            </div>
        </div>
    </div>
</div>