<div class="row">
    <h5 class="mb-3">Dashboard</h5>
    <div class="col-sm-12 mb-5">
        <div class="card">
            <div class="card-header">
                <h5>Grafik Berdasarkan Tanggal Submit</h5>
            </div>
            <div class="card-body">
                <canvas id="myChart" width="400" height="200"></canvas>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-12">
        <div class="card">
            <div class="card-header">
                <h5>Grafik Berdasarkan Status</h5>
            </div>
            <div class="card-body">
                <canvas id="status" width="100" height="200"></canvas>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-12">
        <div class="card">
            <div class="card-header">
                <h5>Grafik Berdasarkan Tanggal Vaksin</h5>
            </div>
            <div class="card-body">
                <canvas id="chartTanggal" width="100" height="200"></canvas>
            </div>
        </div>
    </div>
    <div class="col-sm-12 mt-5">
        <div class="card">
            <div class="card-header">
                <h5>Grafik Berdasarkan Usia</h5>
            </div>
            <div class="card-body">
                <canvas id="myUsia" width="400" height="200"></canvas>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-12 mt-4">
        <div class="card">
            <div class="card-header">
                <h5>Matrik Peserta Vaksin Berdasarkan Jam</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tr>
                            <td></td>
                            <td>07 Agustus 2021</td>
                            <td>08 Agustus 2021</td>
                            <td>09 Agustus 2021</td>
                        </tr>
                        <?php foreach ($jam as $key => $value) { ?>
                            <tr>
                                <td><?= $value->detailJam ?></td>
                                <td>
                                    <?php $sabtu = $this->db->get_where('peserta_v1_fix', ['tanggal' => '2021-08-07', 'jam' => $value->idJam])->num_rows();
                                    if ($sabtu > 90) {
                                        $lbl_sbt = 'danger';
                                    } else {
                                        $lbl_sbt = 'success';
                                    }
                                    ?>
                                    <span class="badge badge-<?= $lbl_sbt ?>"><?php echo $sabtu; ?></span>

                                </td>
                                <td>
                                    <?php $minggu = $this->db->get_where('peserta_v1_fix', ['tanggal' => '2021-08-08', 'jam' => $value->idJam])->num_rows();
                                    if ($minggu > 90) {
                                        $lbl_sbt = 'danger';
                                    } else {
                                        $lbl_sbt = 'success';
                                    }
                                    ?>
                                    <span class="badge badge-<?= $lbl_sbt ?>"><?php echo $minggu; ?></span>

                                </td>
                                <td>
                                    <?php $senin = $this->db->get_where('peserta_v1_fix', ['tanggal' => '2021-08-09', 'jam' => $value->idJam])->num_rows();
                                    if ($senin > 90) {
                                        $lbl_sbt = 'danger';
                                    } else {
                                        $lbl_sbt = 'success';
                                    }
                                    ?>
                                    <span class="badge badge-<?= $lbl_sbt ?>"><?php echo $senin; ?></span>

                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>