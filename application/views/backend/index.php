<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pendaftaran Vaksin DPD IKA Undip DKI Jakarta</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900|Staatliches" rel="stylesheet">
    <link rel="stylesheet" href="<?= asset('vendors/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/OwlCarousel/css/owl.carousel.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/datepicker/css/bootstrap-datepicker.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/OwlCarousel/css/owl.theme.default.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/easyautocomplate/easy-autocomplete.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/easyautocomplate/easy-autocomplete.themes.min.css') ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="<?= asset('css/style.css') ?>">
</head>

<body>
    <header>
        <div class="top-nav">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs text-left">
                        <ul class="list-inline list-inline-top mb-0 py-2 font-size-13-px">
                            <li class="d-inline mr-4">
                                <a href="">
                                    <span class="text-white-50">info@ikaundipjakarta.org</span>
                                </a>
                            </li>
                            <li class="d-inline mr-5">
                                <a href="">
                                    <span class="text-white-50">0815-1177-7999</span>
                                </a>
                            </li>
                            <li class="d-inline mr-3">
                                <a href="">
                                    <span class="text-white-50"><i class="fa fa-facebook" aria-hidden="true"></i></span>
                                </a>
                            </li>
                            <li class="d-inline mr-3">
                                <a href="">
                                    <span class="text-white-50"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
                                </a>
                            </li>
                            <li class="d-inline mr-3">
                                <a href="">
                                    <span class="text-white-50"><i class="fa fa-instagram" aria-hidden="true"></i></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 text-right pull-right ">
                        <ul class="list-inline list-inline-top mb-0 py-2 font-size-13-px text-right position-absolute right-0">
                            <?php if (!empty($this->session->userdata('fullname'))) { ?>

                                <li class="d-inline">
                                    <span class="text-white"><?= strtoupper($this->session->userdata('fullname')) ?></span>
                                </li>
                                <li class="d-inline ml-3">
                                    <a href="<?= site_url('login/destroy') ?>" class="text-white">Keluar</a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light" id="nav-custome">
            <a class="navbar-brand" href="#">
                <img src="<?= asset('image/logo-header-1.png') ?>">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?= site_url('dashboard') ?>">DASHBOARD</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?= site_url('alumni') ?>">ALumni</a>
                    </li>
                </ul>
                <ul class="navbar-nav mr-0">
                    <li class="nav-item dropdown text-center">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Vaksin
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?= site_url('vaksin') ?>">Semua Data</a>
                            <a class="dropdown-item" href="<?= site_url('vaksin/byTanggal') ?>">By Tanggal</a>
                            <a class="dropdown-item" href="<?= site_url('vaksin/byUsia') ?>">Usia</a>

                        </div>
                    </li>

                    <li class="nav-item text-center">
                        <a class="nav-link" href="<?= site_url('vaksin/downloadExcel') ?>">Export</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="content pb-5">
        <div class="container">
            <?php $this->load->view('backend/pages/' . $pages); ?>

        </div>
    </div>
    <footer class="py-3 text-center bg-ika-undip position-relative bottom-0 w-100 text-white mt-5">
        <div class="contrainer">
            <p class="mb-0"> @<?= date('Y') ?> | IKA Universitas Diponegoro DPD DKI Jakarta</p>
        </div>
    </footer>
    <div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Peserta Vaksin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= site_url('vaksin/update') ?>" method="post">

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Alumni</label>
                                    <input type="text" name="namaAlumni" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tanggal Vaksin</label>
                                    <input type="text" name="tglVaksin" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Jam</label>
                                    <?php $getJam = $this->db->get('jam')->result(); ?>
                                    <select name="jam" class="form-control sel-jam">
                                        <?php foreach ($getJam as $key => $value) { ?>
                                            <option value="<?= $value->idJam ?>"><?= $value->detailJam ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="text" name="nik" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nomor Telefon</label>
                                    <input type="text" name="noHp" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" class="form-control status">
                                        <option value="0">Pilih Status</option>
                                        <option value="1">Alumni</option>
                                        <option value="2">Keluarga Alumni</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" name="tanggalLahir" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="detailModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Peserta Vaksin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= site_url('peserta/update') ?>" method="post">

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" name="id">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" class="form-control">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tanggal Vaksin</label>
                                    <input type="text" name="tglVaksin" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Jam</label>
                                    <?php $getJam = $this->db->get('jam')->result(); ?>
                                    <select name="jam" class="form-control sel-jam">
                                        <?php foreach ($getJam as $key => $value) { ?>
                                            <option value="<?= $value->idJam ?>"><?= $value->detailJam ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nomor Telefon</label>
                                    <input type="text" name="noHp" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kehadiran</label>
                                    <select name="hadir" id="hadir" class="form-control">
                                        <option value="0">Pilih Kehadiran</option>
                                        <option value="1">Ya</option>
                                        <option value="2">Tidak</option>
                                        <option value="3">Sudah Terdaftar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="jam">Alasan</label>
                                    <select name="alasan" id="sel_alasan" class="form-control">
                                        <?php $alasan = $this->db->get('alasan')->result(); ?>
                                        <option value="0">Pilih alasan</option>
                                        <?php foreach ($alasan as $key => $value) { ?>
                                            <option value="<?= $value->idAlasan ?>"><?= $value->detailAlasan ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Peserta Vaksin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= site_url('peserta/add') ?>" method="post">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" class="form-control">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Tanggal Vaksin</label>
                                    <?php $arr_hari = ["2021-08-07", "2021-08-08", "2021-08-09"]; ?>
                                    <select name="tanggal" class="form-control">
                                        <?php foreach ($arr_hari as $key => $value_tanggal) { ?>
                                            <option value="<?= date('Y-m-d', strtotime($value_tanggal)) ?>"><?= date('Y-m-d', strtotime($value_tanggal)) ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Nomor Telefon</label>
                                    <input type="text" name="noHp" class="form-control">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="hapusModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Peserta Vaksin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= site_url('vaksin/hapus') ?>" method="post">
                    <input type="hidden" name="id">
                    <div class="modal-body">
                        <p>
                            Apakah Anda Akan menghapus data atas nama <span class="font-weight-bold nama"></span>?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        var url = '<?= site_url('welcome') ?>';
    </script>
    <script src="<?= asset('js/jquery-3.4.1.min.js') ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="<?= asset('vendors/OwlCarousel/js/owl.carousel.js') ?>"></script>
    <script src="<?= asset('vendors/datepicker/js/bootstrap-datepicker.min.js') ?>"></script>
    <script src="<?= asset('vendors/easyautocomplate/jquery.easy-autocomplete.min.js') ?>"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="<?= asset('js/app.js') ?>"></script>
    <script>
        var url = "<?= site_url() ?>";
        $(document).ready(function() {
            <?php if ($this->session->flashdata('show') == 1) { ?>

                Swal.fire({
                    title: '<?php echo $this->session->flashdata('title'); ?>',
                    text: '<?php echo $this->session->flashdata('message'); ?>',
                    icon: '<?php echo $this->session->flashdata('type'); ?>',
                    confirmButtonText: 'Tutup'
                })
            <?php } ?>
            $('#tglFilter').change(function() {
                window.location.href = url + 'vaksin/byTanggal/' + $(this).val();
            })
            $('.btn-detail').click(function() {
                var id = $(this).attr('data-id');
                $.ajax({
                    method: 'POST',
                    url: url + 'vaksin/detail',
                    data: {
                        id,
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#detailModal input[name=nama]').val(data['nama']);
                        $('#detailModal input[name=namaAlumni]').val(data['namaAlumni']);
                        $('#detailModal input[name=id]').val(data['id']);
                        $('#detailModal input[name=tglVaksin]').val(data['tglVaksin']);
                        $('#detailModal input[name=noHp]').val(data['noHp']);
                        $('#detailModal input[name=nik]').val(data['nik']);
                        $('#detailModal input[name=tanggalLahir]').val(data['tanggalLahir']);
                        $('#detailModal .sel-jam').val(data['jam']);
                        $('#detailModal .status').val(data['status']);
                        $('#detailModal').modal();
                    },
                    error: function(XMLHttpRequest) {
                        console.log(XMLHttpRequest);
                    }
                });
            });
            $('.btn-detail-2').click(function() {
                var id = $(this).attr('data-id');
                $.ajax({
                    method: 'POST',
                    url: url + 'peserta/detail',
                    data: {
                        id,
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#detailModal2 input[name=nama]').val(data['nama']);
                        $('#detailModal2 input[name=id]').val(data['idVaksinP']);
                        $('#detailModal2 input[name=tglVaksin]').val(data['tanggal']);
                        $('#detailModal2 input[name=noHp]').val(data['no_hp']);
                        $('#detailModal2 .sel-jam').val(data['jam']);

                        $('#detailModal2').modal();
                    },
                    error: function(XMLHttpRequest) {
                        console.log(XMLHttpRequest);
                    }
                });
            });
            $('.btn-hapus').click(function() {
                $('#hapusModal .nama').html($(this).attr('data-nama'));
                $('#hapusModal input[name="id"]').val($(this).attr('data-id'));
                $('#hapusModal').modal();
            })

            $('.btn-add').click(function() {
                $('#addModal').modal();
            })
        })

        <?php if ($pages == 'dashboard') {
            $jmlTanggal = [];
            $lblTanggal = [];
            foreach ($getTanggal->result() as $key => $value) {
                $jmlTanggal[] = $value->jumlah;
                $lblTanggal[] =  date('d-m', strtotime($value->hari));
            }
        ?>
            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: <?= json_encode($lblTanggal) ?>,
                    datasets: [{
                        label: ' ',
                        data: <?= json_encode($jmlTanggal) ?>,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            <?php
            $jmlStatus = [];
            $lblStatus = [];
            foreach ($getStatus->result() as $key => $value) {

                $jmlStatus[] = $value->jumlah;
                if ($value->status == 1) {
                    $lblStatus[] = 'Alumni';
                } else {
                    $lblStatus[] = 'Keluarga';
                }
            }
            ?>
            var ctx = document.getElementById('status').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: <?= json_encode($lblStatus) ?>,
                    datasets: [{
                        label: 'Jumlah',
                        data: <?= json_encode($jmlStatus) ?>,
                        backgroundColor: [
                            'rgb(255, 99, 132)',
                            'rgb(54, 162, 235)',
                            'rgb(255, 205, 86)'
                        ],

                        borderWidth: 1
                    }]
                }
            });
            <?php
            $jmlVaksin = [];
            $lblVaksin = [];
            foreach ($getVaksin->result() as $key => $value) {
                $jmlVaksin[] = $value->jumlah;
                $lblVaksin[] = date('d-m', strtotime($value->hari));
            }
            ?>
            var ctx = document.getElementById('chartTanggal').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: <?= json_encode($lblVaksin) ?>,
                    datasets: [{
                        label: 'Jumlah',
                        data: <?= json_encode($jmlVaksin) ?>,
                        backgroundColor: [
                            'rgb(255, 99, 132)',
                            'rgb(54, 162, 235)',
                            'rgb(255, 205, 86)'
                        ],

                        borderWidth: 1
                    }]
                }
            });
            <?php
            $arr_usia = [];
            $lblUsia = [];
            foreach ($usia as $key => $value) {
                $lblUsia[] = $value->label;
                $cekData = $this->db->query('select * from anggota where usia >= "' . $value->min . '" and usia <= "' . $value->max . '"')->num_rows();
                $arr_usia[] = $cekData;
            }
            ?>
            var ctx = document.getElementById('myUsia').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: <?= json_encode($lblUsia) ?>,
                    datasets: [{
                        label: ' ',
                        data: <?= json_encode($arr_usia) ?>,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        <?php } ?>
    </script>
</body>

</html>