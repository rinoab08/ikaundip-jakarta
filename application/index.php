<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pendaftaran Vaksin DPD IKA Undip DKI Jakarta</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900|Staatliches" rel="stylesheet">
    <link rel="stylesheet" href="<?= asset('vendors/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/OwlCarousel/css/owl.carousel.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/datepicker/css/bootstrap-datepicker.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/OwlCarousel/css/owl.theme.default.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/easyautocomplate/easy-autocomplete.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/easyautocomplate/easy-autocomplete.themes.min.css') ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="<?= asset('css/style.css') ?>">
</head>

<body>
    <header>
        <div class="top-nav">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs text-left">
                        <ul class="list-inline list-inline-top mb-0 py-2 font-size-13-px">
                            <li class="d-inline mr-4">
                                <a href="">
                                    <span class="text-white-50">info@ikaundipjakarta.org</span>
                                </a>
                            </li>
                            <li class="d-inline mr-5">
                                <a href="">
                                    <span class="text-white-50">0815-1177-7999</span>
                                </a>
                            </li>
                            <li class="d-inline mr-3">
                                <a href="">
                                    <span class="text-white-50"><i class="fa fa-facebook" aria-hidden="true"></i></span>
                                </a>
                            </li>
                            <li class="d-inline mr-3">
                                <a href="">
                                    <span class="text-white-50"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
                                </a>
                            </li>
                            <li class="d-inline mr-3">
                                <a href="">
                                    <span class="text-white-50"><i class="fa fa-instagram" aria-hidden="true"></i></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 text-right pull-right ">
                        <ul class="list-inline list-inline-top mb-0 py-2 font-size-13-px text-right position-absolute right-0">
                            <li class="d-inline">
                                <form class="form-inline custome-form">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Pencarian" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-outline-secondary" type="button" id="button-addon1"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light" id="nav-custome">
            <a class="navbar-brand" href="#">
                <img src="<?= asset('image/logo-header-1.png') ?>">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item text-center">
                        <a class="nav-link" href="#">NEWS</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="#">DIREKTORI ALUMNI</a>
                    </li>
                </ul>
                <ul class="navbar-nav mr-0">
                    <li class="nav-item text-center">
                        <a class="nav-link" href="#">info</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="#">tentang kami</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="content pb-5">
        <div class="container">
            <?php $getPage = $this->db->get_where('testing', ['idTeast' => 1])->row(); ?>
            <div class="carousel slide" data-ride="carousel" id="form-detail">
                    <div class="carousel-inner">

                        <div class="box-progress mb-4 hidden-xs">
                            <div class="progress">
                                <div class="circle progres_1 done">
                                    <span class="label">1</span>
                                    <span class="title">Waktu</span>
                                </div>
                                <span class="bar progres_2" data-progres="2"></span>
                                <div class="circle progres_2" data-progres="2">
                                    <span class="label">2</span>
                                    <span class="title">Identitas</span>
                                </div>
                                <span class="bar progres_3" data-progres="3"></span>
                                <div class="circle progres_3" data-progres="3">
                                    <span class="label">3</span>
                                    <span class="title">Riwayat Kesehatan</span>
                                </div>
                                <span class="bar progres_4" data-progres="4"></span>
                                <div class="circle progres_4" data-progres="4">
                                    <span class="label">4</span>
                                    <span class="title">Persetujuan</span>
                                </div>
                            </div>

                        </div>
                        <form action="<?= site_url('welcome/submitForm') ?>" method="POST" <?= ($getPage->status == 1) ? 'id="formId"' : ''?>>
                            <div class="row">
                                <div class="col-md-8 col-12 mx-auto">
                                    <div class="px-4">
                                        <div class="alert alert-primary mb-3" role="alert">
                                            <strong>Kontak Person</strong><br>
                                            Apabila 15 menit setelah mengirim form Anda tidak mendapatkan Pesan Konfirmasi di WhatsApp, silahkan menghubungi Call Center di 62812-8577-0108 & 62812-1207-4651
                                        </div>
                                    </div>
                                    <div class="carousel-item px-4 active">
                                        <div class="row">
                                            
                                            <?php if ($getPage->status == 1) { ?>

                                            <div class="col-sm-12 mx-auto">
                                                <div class="alert alert-success" role="alert">
                                                    <h4 class="alert-heading">Perhatian</h4>
                                                    <p>Pendaftaran Vaksin Tahap Pertama IKA Undip DPD DKI Jakarta Telah Ditutup</p>

                                                </div>
                                            </div>

                                        <?php } else { ?>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="alert alert-primary" role="alert">
                                                        <strong>Pilih Jadwal</strong><br>
                                                        Jadwal Vaksin yang Tersedia diantaranya
                                                        <ul>

                                                            <?php
                                                            $arr_hari = ["2021-08-07", "2021-08-08","2021-08-09"];
                                                            foreach ($arr_hari as $key => $value) {
                                                                $get_jml = $this->db->get_where('anggota', ['tglVaksin' => $value]);
                                                                if ($get_jml->num_rows() < 800) {

                                                            ?>
                                                                    <li> <?= date('d-m-Y', strtotime($value)) ?> :
                                                                        <?php
                                                                        foreach ($jam as $key_j => $value_j) {
                                                                            $get_jml = $this->db->get_where('anggota', ['tglVaksin' => $value, 'jam' => $value_j->idJam]);
                                                                            if ($get_jml->num_rows() < 125) {
                                                                                echo $value_j->detailJam . '; ';
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </li>
                                                            <?php }
                                                            } ?>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                            <div class="col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Tanggal</label>
                                                    <select name="tglVaksin" id="tglVaksin" class="form-control">
                                                        <option value="0">Pilih Tanggal Vaksin</option>
                                                        <option value="2021-08-07" <?= (set_value('tglVaksin') == "2021-08-07") ? 'selected' : '' ?>>Sabtu, 07 Agustus 2021</option>
                                                        <option value="2021-08-08" <?= (set_value('tglVaksin') == "2021-08-08") ? 'selected' : '' ?>>Minggu, 08 Agustus 2021</option>
                                                        <option value="2021-08-09" <?= (set_value('tglVaksin') == "2021-08-08") ? 'selected' : '' ?>>Senin, 09 Agustus 2021</option>
                                                    </select>
                                                    <span class="text-danger font-sm"><?= form_error('tglVaksin') ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="jam">Jam</label>
                                                    <select name="jam" id="jam" class="form-control">
                                                        
                                                        <?php foreach ($jam as $key => $value) { ?>
                                                            <option value="<?= $value->idJam ?>" <?= (set_value('jam') == $value->idJam) ? 'selected' : '' ?>><?= $value->detailJam ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <span class="text-danger font-sm"><?= form_error('jam') ?></span>
                                                </div>
                                            </div>
                                            <div class="col-12 d-none" id="note">
                                                <div class="form-group">
                                                    <div class="alert alert-danger" role="alert">
                                                        Quota pada hari dan jam tersebut sudah terpenuhi, silahkan pilih lainnya.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 text-right  d-none mt-4" id="tmb-next">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-next" data-id="2">Selanjutnya <i class="fa fa-chevron-circle-right ml-2" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item px-4">
                                        <div class="row">
                                            <div class="col-12 mb-3">
                                                <div class="form-group">
                                                    <div class="alert alert-primary" role="alert">
                                                        <strong>Data Diri</strong><br>
                                                        Lengkapi data pribadi anda dengan sebenar-benarnya
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Status <span class="text-danger font-sm">*</span></label>
                                                <select name="status" class="form-control" id="statusPeserta">

                                                    <option value="1" <?= (set_value('status') == "1") ? 'selected' : '' ?>>Alumni</option>
                                                    <option value="2" <?= (set_value('status') == "2") ? 'selected' : '' ?>>Keluarga Alumni</option>
                                                    <option value="3" <?= (set_value('status') == "3") ? 'selected' : '' ?>>Masyarakat Umum</option>
                                                </select>
                                                <span class="text-danger font-sm"><?= form_error('status') ?></span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-sm-6 col-12">
                                                    <div class="form-group">
                                                        <label for="namaAlumni">Nama Alumni Undip <span class="text-danger font-sm">*</span></label>
                                                        <input type="text" class="form-control" id="namaAlumni" name="namaAlumni" aria-describedby="nama" value="<?= set_value('namaAlumni') ?>">
                                                        <span class="text-muted font-sm">Nama Lengkap Alumni Undip Sesuai KTP</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-12">
                                                    <div class="form-group">
                                                        <label for="fakultasAlumni">Fakuktas Alumni Undip</label>
                                                        <input type="text" class="form-control" name="fakuktas" id="fakultasAlumni" value="<?= set_value('fakultas') ?>">
                                                        <span class="<?= (form_error('fakultas')) ? 'text-danger' : 'text-muted'; ?> font-sm"><?= (form_error('fakultas')) ? form_error('fakultas') : 'Fakultas dari Alumni Undip'; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="penerimavaksin">Nama Penerima Vaksin</label>
                                                    <input type="text" class="form-control" name="nama" id="penerimavaksin" value="<?= set_value('nama') ?>">
                                                    <span class="text-muted font-sm">Nama Lengkap Penerima Vaksin Sesuai KTP</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="jeniskelamin">Jenis Kelamin Penerima Vaksin<span class="text-danger font-sm">*</span></label>
                                                    <select name="jenisKelamin" class="form-control">
                                                        <option value="0">Pilih Jenis Kelamin</option>
                                                        <option value="1">Laki-Laki</option>
                                                        <option value="2">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="exampelInputNIK">Nomor Induk Kependudukan <span class="text-danger font-sm">*</span></label>
                                                    <input type="text" class="form-control" id="exampelInputNIK" name="nik" aria-describedby="nama" value="<?= set_value('nik') ?>">
                                                    <span class="text-danger font-sm"><?= form_error('nik') ?></span>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="exampleInputTanggalLahir">Tanggal Lahir <span class="text-danger font-sm">*</span></label>
                                                    <input type="text" class="form-control use-datepicker" id="exampleInputTanggalLahir" name="tanggalLahir" value="<?= set_value('tanggalLahir') ?>">
                                                    <span class="text-danger font-sm"><?= form_error('tanggalLahir') ?></span>
                                                </div>
                                            </div>

                                            <div class=" col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Alamat <span class="text-danger font-sm">*</span></label>
                                                    <textarea name="alamat" rows="5" class="form-control"><?= set_value('alamat') ?></textarea>
                                                    <span class="<?= (form_error('alamat')) ? 'text-danger' : 'text-muted'; ?> font-sm"><?= (form_error('alamat')) ? form_error('alamat') : 'Alamat Sesuai KTP'; ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-12" id="phone-number">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nomor WhatsApp Penerima Vaksin<span class="text-danger font-sm">* (nomor WA aktif)</span></label>
                                                    <input type="text" class="form-control" id="notelp" name="noHp" value="62<?= set_value('noHp') ?>">
                                                    <span class="text-muted font-sm">Angka 62 menggantikan angka 0 di depan nomor handphone</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Email <span class="text-danger font-sm">*</span></label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" value="<?= set_value('email') ?>">
                                                    <span class="text-danger font-sm"><?= form_error('email') ?></span>
                                                </div>
                                            </div>



                                            <div class=" col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Kategori <span class="text-danger font-sm">*</span></label>
                                                    <select name="kategori" class="form-control">
                                                        <option value="">Pilih Kategori</option>
                                                        <?php foreach ($kategori->result() as $key => $value) { ?>
                                                            <option value="<?= $value->idKategori ?>" <?= (set_value('kategori') == $value->idKategori) ? 'selected' : '' ?>><?= $value->detail ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <span class="text-danger font-sm"><?= form_error('kategori') ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Jenis Pekerjaan </label>
                                                    <input type="text" class="form-control" name="jenisPekerjaan" value="<?= set_value('jenisPekerjaan') ?>">
                                                    <span class="text-danger font-sm"><?= form_error('jenisPekerjaan') ?></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nama Kantor</label>
                                                    <input type="text" class="form-control" name="instansi" value="<?= set_value('instansi') ?>">
                                                    <span class="text-danger font-sm"><?= form_error('instansi') ?></span>
                                                </div>
                                            </div>
                                            <div class=" col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Kabupaten / Kota</label>
                                                    <select name="kabkota" class="form-control use-select2">
                                                        <option value="">Pilih Kabupaten / Kota</option>
                                                        <?php foreach ($kabkota->result() as $key => $value) { ?>
                                                            <option value="<?= $value->idKota ?>" <?= (set_value('kabkota') == $value->idKota) ? 'selected' : '' ?>><?= $value->detailKota ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-6 text-left">
                                                <a href="javascript:void(0)" class="btn btn-secondary btn-prev" data-id="1"><i class="fa fa-chevron-circle-left mr-2" aria-hidden="true"></i>Kembali </a>
                                            </div>
                                            <div class="col-6 text-right">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-next" data-id="3">Selanjutnya <i class="fa fa-chevron-circle-right ml-2" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item px-4">

                                        <div class="row">
                                            <div class="col-12 mb-3">
                                                <div class="form-group">
                                                    <div class="alert alert-primary" role="alert">
                                                        <strong>Riwayat Kesehatan</strong>
                                                        <label>Centang jika pernyataan tersebut sesuai</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $i = 0 ?>
                                            <?php foreach ($screening as $key => $v_screening) { ?>
                                                <?php
                                                if ($v_screening->ket == 1) {
                                                    if ($i == 0) {
                                                        $i++;
                                                ?>
                                                        <h6 class="font-weight-bold">Pertanyaan tambahan bagi sasaran lansia (> 60 Tahun)</h6>
                                                <?php }
                                                }

                                                ?>
                                                <div class="col-12">
                                                    <div class="form-group form-check text-justify">
                                                        <input type="checkbox" class="form-check-input" name="riwayatPenyakit[]" id="<?= 'exampleCheck' . $v_screening->idScreening ?>" value="<?= $v_screening->idScreening ?>">
                                                        <label class="form-check-label" for="<?= 'exampleCheck' . $v_screening->idScreening ?>"><?= $v_screening->deskripsi ?></label>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-6 text-left">
                                                <a href="javascript:void(0)" class="btn btn-secondary btn-prev" data-id="2"><i class="fa fa-chevron-circle-left mr-2" aria-hidden="true"></i>Kembali </a>
                                            </div>
                                            <div class="col-6 text-right">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-next" data-id="4">Selanjutnya <i class="fa fa-chevron-circle-right ml-2" aria-hidden="true"></i></a>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="carousel-item px-4">

                                        <div class="row">
                                            <div class="col-12 mb-3">
                                                <div class="form-group">
                                                    <div class="alert alert-primary" role="alert">
                                                        <strong>Halaman Persetujuan</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group form-check text-justify">
                                                    <input type="checkbox" class="form-check-input" id="persetujuan1" name="aggrement[]" value="1" required>
                                                    <label class="form-check-label" for="persetujuan1">Saya setuju sebagai penerima Vaksinasi Covid-19 dengan ini menyatakan bahwa data isian formulir tersebut di atas saya buat dengan sebenarnya sesuai dengan data yang diberikan pada saat mendaftarkan sebagai penerima vaksinasi Covid-19, apabila ternyata dikemudian hari saya melakukan kebohongan/manipulasi terhadap data tersebut maka saya bersedia menerima sanksi sesuai dengan peraturan perundangan.</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group form-check text-justify">
                                                    <input type="checkbox" class="form-check-input" id="persetujuan2" name="aggrement[]" value="2" required>
                                                    <label class="form-check-label" for="persetujuan2">Saya setuju untuk menjadi penerima Vaksinasi Covid-19 dan bertanggungjawab sepenuhnya terhadap resiko secara mandiri dan melepaskan diri dari tanggungjawab Organisasi</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-6 text-left">
                                                <a href="javascript:void(0)" class="btn btn-secondary btn-prev" data-id="3"><i class="fa fa-chevron-circle-left mr-2" aria-hidden="true"></i>Kembali </a>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button class="btn btn-success" id="btnRegister" type="submit"><i class="fa fa-paper-plane mr-2" aria-hidden="true"></i>Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>


                </div>
            

        </div>
    </div>
    <footer class="py-3 text-center bg-ika-undip position-relative bottom-0 w-100 text-white mt-5">
        <div class="contrainer">
            <p class="mb-0"> @<?= date('Y') ?> | IKA Universitas Diponegoro DPD DKI Jakarta</p>
        </div>
    </footer>
    <script>
        var url = '<?= site_url('welcome') ?>';
    </script>
    <script src="<?= asset('js/jquery-3.4.1.min.js') ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="<?= asset('vendors/OwlCarousel/js/owl.carousel.js') ?>"></script>
    <script src="<?= asset('vendors/datepicker/js/bootstrap-datepicker.min.js') ?>"></script>
    <script src="<?= asset('vendors/easyautocomplate/jquery.easy-autocomplete.min.js') ?>"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="<?= asset('js/app.js?v=6') ?>"></script>
    <script>
        $(document).ready(function() {
            changeStatus();
            var options = {
                url: function(phrase) {
                    return url + '/getAlumni/?phrase=' + phrase + "&format=json";
                },
                getValue: "nama",
                list: {
                    onClickEvent: function() {
                        if ($('#status').val() != 2) {
                            $('#penerimavaksin').val($(".nama-alumni").getSelectedItemData().nama);
                        }

                    }
                }
            };
            $(".nama-alumni").easyAutocomplete(options);

            <?php if ($this->session->flashdata('show') == 1) { ?>

                Swal.fire({
                    title: '<?php echo $this->session->flashdata('title'); ?>',
                    text: '<?php echo $this->session->flashdata('message'); ?>',
                    icon: '<?php echo $this->session->flashdata('type'); ?>',
                    confirmButtonText: 'Tutup'
                })
            <?php } ?>
        })

        function changeStatus() {
            $('#status').change(function() {
                if ($(this).val() == '2') {
                    $('#penerimavaksin').removeAttr('readonly');

                } else {
                    $('#penerimavaksin').attr('readonly', 'true');
                }
            })
        }
        $('#btnRegister').click(function(e) {

            let wa = $(this).closest('form').find('input[name="noHp"]').val();
            let wa_2firstnum = wa.substr(0, 2);
            let wa_2secon = wa.substr(0, 4);
            let wa_2thirdt = wa.substr(0, 3);


            if (wa_2thirdt != '628') {
                alert('Format "Nomor Whatsapp" Anda salah. Gunakan format "628xxxxxxxxx"');
                return false;
            }
            return true;
        });
    </script>
</body>

</html>