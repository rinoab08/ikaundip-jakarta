<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_Alumni extends CI_Model
{
    var $table = 'alumni';
    var $primary = 'idAlumni';

    function getdata($limit, $offset)
    {
        $this->db->order_by($this->primary, 'asc');
        $join = $this->db->get($this->table, $limit, $offset);
        return $join;
    }

    function get_all()
    {
        $result = $this->db->get($this->table);
        return $result;
    }

    function get_order($column, $tipe)
    {
        $this->db->order_by($column, $tipe);
        $result = $this->db->get($this->table);
        return $result;
    }

    function get_condition($condition)
    {
        $this->db->where($condition);
        $result = $this->db->get($this->table);

        return $result;
    }

    function insert($object)
    {
        return $this->db->insert($this->table, $object);
    }

    function update($id, $object)
    {
        $this->db->where($this->primary, $id);
        return $this->db->update($this->table, $object);
    }

    function delete($value)
    {
        return $this->db->delete($this->table, array($this->primary => $value));
    }

    function FailedAll()
    {
        $result = $this->db->get('failed_data');
        return $result;
    }

    function FailedData($limit, $offset)
    {
        
        $join = $this->db->get('failed_data', $limit, $offset);
        return $join;
    }
    
     function updateFailed($id, $object)
    {
        $this->db->where('id', $id);
        return $this->db->update('failed_data', $object);
    }
}

/* End of file Anggota.php */
