<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Anggota extends CI_Model
{
    var $table = 'anggota';
    var $primary = 'id';

    function getdata($limit, $offset)
    {
        $this->db->order_by('created_date', 'desc');
        $this->db->join('jam', 'jam.idJam = anggota.jam');
        $join = $this->db->get($this->table, $limit, $offset);
        return $join;
    }
    
    function getUsia($limit, $offset)
    {
        $this->db->order_by('usia', 'asc');
        $this->db->join('jam', 'jam.idJam = anggota.jam');
        $join = $this->db->get($this->table, $limit, $offset);
        return $join;
    }

    function get_all()
    {
        $this->db->order_by('tglVaksin', 'asc');
        $this->db->order_by('jam', 'asc');
        $this->db->join('jam', 'jam.idJam = anggota.jam');
        
        $result = $this->db->get($this->table);

        return $result;
    }
    
    function getLike($query){
        $this->db->like('nama', $query, 'both');
         $this->db->join('jam', 'jam.idJam = anggota.jam');
		$result = $this->db->get($this->table);
		return $result;
    }
    
     function getLikeLimit($query,$limit, $offset){
        $this->db->like('nama', $query, 'both');
         $this->db->join('jam', 'jam.idJam = anggota.jam');
		$join = $this->db->get($this->table, $limit, $offset);
        return $join;
    }

    function get_order($column, $tipe)
    {
        $this->db->order_by($column, $tipe);
        $result = $this->db->get($this->table);
        return $result;
    }

    function get_condition($condition)
    {
        $this->db->where($condition);
        $this->db->join('jam', 'jam.idJam = anggota.jam');
        $result = $this->db->get($this->table);

        return $result;
    }
    
    function getdataCondtion($condition, $limit, $offset)
    {
        $this->db->where($condition);
        $this->db->order_by('jam', 'asc');
        $this->db->join('jam', 'jam.idJam = anggota.jam');
        $join = $this->db->get($this->table, $limit, $offset);
        return $join;
    }
    
    function insert($object)
    {
        return $this->db->insert($this->table, $object);
    }

    function update($id, $object)
    {
        $this->db->where($this->primary, $id);
        return $this->db->update($this->table, $object);
    }

    function delete($value)
    {
        return $this->db->delete($this->table, array($this->primary => $value));
    }

    function getChart()
    {
        $this->db->select('date(created_date) as hari, count(nik) as jumlah');
        $this->db->group_by('date(created_date)');
        $this->db->order_by('hari', 'asc');

        $result = $this->db->get('anggota');

        return $result;
    }

    function getVaksin()
    {
        $this->db->select('date(tglVaksin) as hari, count(nik) as jumlah');
        $this->db->group_by('date(tglVaksin)');
        $this->db->order_by('hari', 'asc');
        $result = $this->db->get('anggota');

        return $result;
    }

    function getStatus()
    {
        $this->db->select('status, count(nik) as jumlah');
        $this->db->group_by('status');
        $result = $this->db->get('anggota');

        return $result;
    }
}

/* End of file Anggota.php */
