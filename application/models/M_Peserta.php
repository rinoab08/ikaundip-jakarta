<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_Peserta extends CI_Model
{
    var $table = 'peserta_v1_fix';
    var $primary = 'idVaksinP';

    function getdata($limit, $offset, $condition = null)
    {
        if ($condition != null) {
            $this->db->where($condition);
        }
        $this->db->order_by('jam', 'asc');
        $this->db->order_by('tanggal', 'asc');

        // $this->db->group_by(['nama', 'no_hp']);


        $join = $this->db->get($this->table, $limit, $offset);
        return $join;
    }

    function get_all()
    {
        // $this->db->group_by(['nama', 'no_hp']);
        $result = $this->db->get($this->table);
        return $result;
    }

    function get_order($column, $tipe)
    {
        $this->db->order_by($column, $tipe);
        $result = $this->db->get($this->table);
        return $result;
    }

    function get_condition($condition)
    {
        $this->db->where($condition);
        $result = $this->db->get($this->table);

        return $result;
    }

    function get_condition_group($condition)
    {
        $this->db->where($condition);
        $this->db->group_by('nama');
        $this->db->group_by('no_hp');
        $result = $this->db->get($this->table);

        return $result;
    }

    function insert($object)
    {
        return $this->db->insert($this->table, $object);
    }

    function update($id, $object)
    {
        $this->db->where($this->primary, $id);
        return $this->db->update('jateng', $object);
    }

    function update_custome($condition, $object)
    {
        $this->db->where($condition);
        return $this->db->update($this->table, $object);
    }

    function delete($value)
    {
        return $this->db->delete($this->table, array($this->primary => $value));
    }

    function FailedAll()
    {
        $result = $this->db->get('failed_data');
        return $result;
    }

    function FailedData($limit, $offset)
    {

        $join = $this->db->get('failed_data', $limit, $offset);
        return $join;
    }

    function updateFailed($id, $object)
    {
        $this->db->where('id', $id);
        return $this->db->update('failed_data', $object);
    }

    function getLike($query)
    {
        $this->db->like('nama', $query, 'both');

        $result = $this->db->get($this->table);
        return $result;
    }

    function getLikeLimit($query, $limit, $offset)
    {
        $this->db->like('nama', $query, 'both');

        $join = $this->db->get($this->table, $limit, $offset);
        return $join;
    }

    function getDataJateng($limit, $offset)
    {
        $join = $this->db->get('jateng', $limit, $offset);
        return $join;
    }

    function getDataJatengAll()
    {
        $join = $this->db->get('jateng');
        return $join;
    }
}

/* End of file Anggota.php */
