<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Model
{
    var $table = 'kategori';
    var $primary = 'idKategori';

    function getdata($limit, $offset)
    {
        $this->db->order_by($this->primary, 'asc');
        $join = $this->db->get($this->table, $limit, $offset);
        return $join;
    }

    function get_all()
    {
        $result = $this->db->get($this->table);
        return $result;
    }

    function get_order($column, $tipe)
    {
        $this->db->order_by($column, $tipe);
        $result = $this->db->get($this->table);
        return $result;
    }

    function get_condition($condition)
    {
        $this->db->where($condition);
        $result = $this->db->get($this->table);

        return $result;
    }

    function insert($object)
    {
        return $this->db->insert($this->table, $object);
    }

    function update($id, $object)
    {
        $this->db->where($this->primary, $id);
        return $this->db->update($this->table, $object);
    }

    function delete($value)
    {
        return $this->db->delete($this->table, array($this->primary => $value));
    }
}

/* End of file Anggota.php */
