<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('upload');
        check_login();
        $this->load->model(['Kategori', 'Anggota', 'Kabkota']);
    }

    public function index()
    {

        $data['getStatus'] = $this->Anggota->getStatus();
        $data['getTanggal'] = $this->Anggota->getChart();
        $data['getVaksin'] = $this->Anggota->getVaksin();
        $data['jam']       = $this->db->get('jam')->result();
        $data['usia']      = $this->db->get('usia')->result();
        $data['pagetitle'] = 'Dashboard';
        $data['pages'] = 'dashboard';

        $this->load->view('backend/index', $data);
    }
}

/* End of file Dashboard.php */
