<div class="row">
    <div class="col-sm-12">
        <h5 class="mb-3">Data Peserta Vaksin Pertama </h5>
    </div>
    <div class="col-sm-4 mb-3 text-center">
        <label>Terdaftar</label>
        <h4><?= $register->num_rows() + $register_upt->num_rows() ?></h4>
    </div>
    <div class="col-sm-4 mb-3 text-center">
        <label>On The Spot</label>
        <h4><?= $ots->num_rows() ?></h4>
    </div>
    <div class="col-sm-4 mb-3 text-center">
        <label>Nomor Kosong</label>
        <h4><?= $no_empty->num_rows() ?></h4>
    </div>
    <div class="col-sm-12 mb-3">
        <a href="<?= site_url('peserta/Export') ?>" class="btn btn-success">Export Peserta</a>
    </div>
    <div class="col-sm-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tiket</th>
                        <th scope="col">Nama Alumni</th>
                        <th scope="col">No. Telp</th>
                        <th scope="col">Tanggal Vaksin</th>
                        <th scope="col">Jam</th>
                        
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1 + $this->uri->segment(3); ?>
                    <?php foreach ($peserta->result() as $key => $value) { ?>
                        <tr>
                            <th scope="row"><?= $i ?></th>
                            <?php $i++; ?>
                            <td><?= $value->tiket ?></td>
                            <?php
                                $checkData = $this->db->get_where('anggota', ['tglVaksin' => date('Y-m-d', strtotime($value->tanggal)), 'nama' => $value->nama]);
                                
                            ?>
                            <td>
                                <?= $value->nama ?>
                                <?php 
                                    if ($checkData->num_rows() != 0 && $value->status == 0) {
                                        $getRow = $checkData->row();
                                        $this->M_Peserta->update($value->idVaksinP, ['jam' => $getRow->jam, 'status' => 1]);
                                        echo '<br><span class="badge badge-warning">Pendaftaran Baru</span>';
                                    }
                                ?>
                            </td>
                            <td><?= $value->no_hp ?></td>
                            <td><?= date('d-m-Y', strtotime($value->tanggal)) ?></td>
                            <?php $jam= $value->jam;
                                if (!empty($value->jam)) {
                                    $getJam=$this->db->get_where('jam', ['idJam' => $value->jam])->row();
                                    $jam = $getJam->detailJam;
                                }
                            ?>
                            <td><?= $jam ?></td>
                            <td><?= ($value->blast == 0) ? 'pending' : 'terkirim'; ?></td>
                            <td>
                            <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-detail-2" data-id="<?= $value->idVaksinP ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </td>

                        </tr>
                    <?php } ?>


                </tbody>

            </table>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $paginator ?>
            </div>
        </div>
    </div>
</div>