
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('upload');

		$this->load->model(['Kategori', 'Anggota', 'Kabkota', 'M_Peserta']);
	}

	public function index()
	{
		$data['jam'] = $this->db->get('jam')->result();
		$data['kategori'] = $this->Kategori->get_all();
		$data['kabkota'] = $this->Kabkota->get_all();
		$data['screening'] = $this->db->get('screening')->result();
		$this->load->view('frontend/index', $data);
	}

	public function cekQuota()
	{
		$getQuota = $this->db->get_where('peserta_v1_fix', ['tanggal' => $_POST['tanggal'], 'jam' => $_POST['jam']]);
		echo $getQuota->num_rows();
	}

	public function submitForm()
	{
		$this->form_validation->set_rules('tglVaksin', 'tglVaksin', 'trim|required');
		$this->form_validation->set_rules('jam', 'jam', 'trim|required');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('nik', 'nik', 'trim|required');
		$this->form_validation->set_rules('jenisKelamin', 'jenisKelamin', 'required');
		// $this->form_validation->set_rules('tempatLahir', 'tempatLahir', 'required');
		$this->form_validation->set_rules('tanggalLahir', 'tanggalLahir', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('noHp', 'noHp', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		// $this->form_validation->set_rules('fakuktas', 'fakuktas', 'required');
		// 		$this->form_validation->set_rules('jenisPekerjaan', 'jenisPekerjaan', 'required');
		// 		$this->form_validation->set_rules('instansi', 'instansi', 'required');
		// 		$this->form_validation->set_rules('kabkota', 'kabkota', 'required');
		$kesehatan = '';
		if (isset($_POST['riwayatPenyakit'])) {
			$kesehatan = implode(';', $_POST['riwayatPenyakit']);
		}

		$aggrement = '';
		if (isset($_POST['aggrement'])) {
			$aggrement = implode(';', $_POST['aggrement']);
		}
		if ($this->form_validation->run() == TRUE) {
			$msg = 'Pesan Terkirim';
			$data = [
				'created_date' => date('Y-m-d H:i:s'),
				'tglVaksin'	=> date('Y-m-d', strtotime(set_value('tglVaksin'))),
				'jam' => set_value('jam'),
				'namaAlumni' => set_value('namaAlumni'),
				'nik' => set_value('nik'),
				'nama' => set_value('nama'),
				'jenisKelamin' => set_value('jenisKelamin'),
				'tanggalLahir' => date('Y-m-d', strtotime(set_value('tanggalLahir'))),
				'alamat' => set_value('alamat'),
				'noHp' => str_replace([' ', '-'], ['', ''], set_value('noHp')),
				'email' => set_value('email'),
				'jenisPekerjaan' => set_value('jenisPekerjaan'),
				'instansi' => set_value('instansi'),
				'kodeKategori' => set_value('kodeKategori'),
				'status' => set_value('status'),
				'lokasiInstansi' => set_value('lokasiInstansi'),
				'fakultas' => set_value('fakultas'),
				'riwayatPenyakit' => $kesehatan,
				'aggrement' => $aggrement
			];
			$this->db->insert('anggota', $data);
			$lastId = $this->db->insert_id();
			// send_chat($_POST['noHp'], $msg);
			$getJam = $this->db->get_where('jam', ['idJam' => $_POST['jam']])->row();
			$this->session->set_flashdata('show', '1');
			$this->session->set_flashdata('type', 'success');
			$this->session->set_flashdata('title', 'Perhatian!');
			$this->session->set_flashdata('message', 'Pendaftaran Vaksin Berhasil Dilakukan');
			$hari = getHari(date('D', strtotime(set_value('tglVaksin'))));
			$bulan = getBulan(date('m', strtotime(set_value('tglVaksin'))));
			$msg = '*DPD IKA Undip DKI Jakarta Peduli VAKSINASI Usia 12 Tahun Keatas* -- Hallo *' . $_POST['nama'] . '*--Selamat Pendaftaran Anda berhasil, berikut rincian jadwal vaksin Anda.--Hari, tanggal : *' . $hari . ', ' . date('d', strtotime(set_value('tglVaksin'))) . ' ' . $bulan . ' 2021*-Pukul  : *' . str_replace('-', '%2D', $getJam->detailJam)  . '* -Tempat : Kuningan City Lt. 2 Unit 3%2D6-Jl. Prof. Dr. Satrio Kav. 18 -Jakarta Selatan';
			if ($lastId % 2 == 1) {
				customerService1($_POST['noHp'], $msg);
			} else {
				customerService2($_POST['noHp'], $msg);
			}
			redirect('welcome/index');
		} else {
			$this->session->set_flashdata('show', '1');
			$this->session->set_flashdata('type', 'error');
			$this->session->set_flashdata('title', 'Perhatian!');
			$this->session->set_flashdata('message', 'Pendaftaran Vaksin Tidak Berhasil Dilakukan');
			$data['jam'] = $this->db->get('jam')->result();
			$data['kategori'] = $this->Kategori->get_all();
			$data['kabkota'] = $this->Kabkota->get_all();
			$data['screening'] = $this->db->get('screening')->result();
			$this->load->view('frontend/index', $data);
		}
	}

	public function getAlumni()
	{
		$getData = $this->db->query('select nama from alumni where nama like "%' . $_GET['phrase'] . '%"')->result();
		echo json_encode($getData);
	}

	public function cekWAblast()
	{
		$msg = '*DPD IKA Undip DKI Jakarta Peduli*--*VAKSINASI Usia 18 Tahun Keatas* -- Dalam rangka bakti sosial dan mensukseskan program pemerintah guna percepatan vaksinasi nasional, DPD IKA UNDIP DKI Jakarta bekerjasama dengan Dinas Kesehatan Provinsi DKI Jakarta dan Kuningan City akan melaksanakan vaksinasi gratis untuk alumni Undip beserta keluarga usia 18 tahun keatas.--Hari, tanggal : Sabtu%2DMinggu, 10%2D11 Juli 2021-Pukul  : 11.00 %2D 19.00 -Tempat : Kuningan City Lt. 2 Unit 3%2D6-Jl. Prof. Dr. Satrio Kav. 18 -Jakarta Selatan-Maps: https://goo.gl/maps/hDN8WbP6Dtvmex3T8 -- Ayo sukseskan program vaksinasi IKA Undip DKI Jakarta, dengan mengisi form bidodata berikut ini-- https://vaksin.ikaundipjakarta.org/ -- 28%2D29 Juni 2021 pendaftaran khusus untuk alumni yang sudah masuk database keanggotaan DPD IKA UNDIP DKI Jakarta.--*Call Center* : -0812%2D1207%2D4651 -0812%2D8577%2D0108';
		$getTes = $this->db->get('testing')->result();
		foreach ($getTes as $key => $value) {
			if ($key % 2 == 1) {
				customerService1($value->nomor, $msg);
			} else {
				customerService2($value->nomor, $msg);
			}
		}
	}

	public function show_page($parameter)
	{
		$this->db->where('idTeast', 1);
		$this->db->update('testing', ['status' => $parameter]);
		redirect('welcome/index');
	}

	public function pendaftaran($tiket)
	{
		$check_data = $this->db->get_where('peserta_v1_fix', ['tiket' => $tiket])->row();
		if ($check_data->status == 1) {

			$this->session->set_flashdata('show', '1');
			$this->session->set_flashdata('type', 'success');
			$this->session->set_flashdata('title', 'Perhatian!');
			$this->session->set_flashdata('message', 'Anda Sudah Terdaftar di Vaksinasi Kedua');
		}
		$data['alasan'] = $this->db->get('alasan')->result();
		$data['jam'] = $this->db->get('jam')->result();
		$data['peserta'] = $this->db->get_where('peserta_v1_fix', ['tiket' => $tiket]);
		$this->load->view('frontend/index-2', $data);
	}

	public function updateData()
	{
		$this->form_validation->set_rules('jam', 'jam', 'trim|required');

		if ($this->form_validation->run() == TRUE) {
			$upt = ['jam' => set_value('jam'), 'status' => 1, 'hadir' => $_POST['hadir'], 'alasan' => $_POST['alasan'], 'txt_alasan' => $_POST['txt_alasan']];
			$this->M_Peserta->update($_POST['idVaksinP'], $upt);


			$getJam = $this->db->get_where('jam', ['idJam' => $_POST['jam']])->row();
			$this->session->set_flashdata('show', '1');
			$this->session->set_flashdata('type', 'success');
			$this->session->set_flashdata('title', 'Perhatian!');
			$this->session->set_flashdata('message', 'Pendaftaran Vaksin Berhasil Dilakukan');
			$hari = getHari(date('D', strtotime(set_value('tglVaksin'))));
			$bulan = getBulan(date('m', strtotime(set_value('tglVaksin'))));
			if ($_POST['hadir'] == 1) {
				# code...
				$msg = '*DPD IKA UNDIP DKI JAKARTA PEDULI VAKSINASI TAHAP KEDUA USIA 12 TAHUN KEATAS* --Hallo *' . $_POST['nama'] . '*--Selamat Pendaftaran Anda berhasil, berikut rincian jadwal *vaksin kedua* Anda.--Hari, tanggal : *' . $hari . ', ' . date('d', strtotime(set_value('tglVaksin'))) . ' ' . $bulan . ' 2021*-Pukul  : *' . str_replace('-', '%2D', $getJam->detailJam)  . '* -Tempat : Kuningan City Lt. 2 Unit 3 %2D 6-Jl. Prof. Dr. Satrio Kav. 18 -Jakarta Selatan--Demi mencegah kerumunan, harap tidak datang terlalu awal maupun terlambat.-Jika datang terlalu awal dari jam seharusnya silakan menunggu di tempat yang telah disediakan Panitia di lantai G depan Coffee Bean, hal ini untuk menghindari penumpukan peserta di lantai 2.';
			} else {
				$msg = '*DPD IKA UNDIP DKI JAKARTA PEDULI VAKSINASI TAHAP KEDUA USIA 12 TAHUN KEATAS* --Hallo *' . $_POST['nama'] . '*--Terimakasih sudah memberikan konfirmasi.';
			}
			if ($_POST['idVaksinP'] % 2 == 1) {
				customerService1($_POST['noHp'], $msg);
			} else {
				customerService2($_POST['noHp'], $msg);
			}
			redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->session->set_flashdata('show', '1');
			$this->session->set_flashdata('type', 'error');
			$this->session->set_flashdata('title', 'Perhatian!');
			$this->session->set_flashdata('message', 'Pendaftaran Anda Tidak Berhasil Dilakukan');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
