<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Alumni extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('upload');
        check_login();
        $this->load->model(['Kategori', 'Anggota', 'Kabkota', 'M_Alumni']);
    }

    public function index()
    {
        $page = $this->uri->segment(3);

        $limit = 30;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->M_Alumni->get_all();
        $set_config = array(
            'base_url' => site_url() . 'Alumni/index',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);

        $this->pagination->initialize($config);
        $data['alumni'] = $this->M_Alumni->getdata($limit, $offset);
        $upt =  $this->M_Alumni->getdata($limit, $offset);
        $msg = '*DPD IKA Undip DKI Jakarta Peduli*--*VAKSINASI Usia 18 Tahun Keatas* -- Dalam rangka bakti sosial dan mensukseskan program pemerintah guna percepatan vaksinasi nasional, DPD IKA UNDIP DKI Jakarta bekerjasama dengan Dinas Kesehatan Provinsi DKI Jakarta dan Kuningan City akan melaksanakan vaksinasi gratis untuk alumni Undip beserta keluarga usia 18 tahun keatas.--Hari, tanggal : Sabtu%2DMinggu, 10%2D11 Juli 2021-Pukul  : 11.00 %2D 19.00 -Tempat : Kuningan City Lt. 2 Unit 3%2D6-Jl. Prof. Dr. Satrio Kav. 18 -Jakarta Selatan-Maps: https://goo.gl/maps/hDN8WbP6Dtvmex3T8 -- Ayo sukseskan program vaksinasi IKA Undip DKI Jakarta, dengan mengisi form bidodata berikut ini-- https://vaksin.ikaundipjakarta.org/ -- 28%2D29 Juni 2021 pendaftaran khusus untuk alumni yang sudah masuk database keanggotaan DPD IKA UNDIP DKI Jakarta.--*Call Center* : -0812%2D1207%2D4651 -0812%2D8577%2D0108';
        foreach ($upt->result() as $key => $value) {
            if ($value->sent == 1) {
                if ($key % 2 == 1) {
                    customerService1($value->noHp, $msg);
                } else {
                    customerService2($value->noHp, $msg);
                }
            }
            $this->M_Alumni->update($value->idAlumni, ['sent' => 0]);
        }
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'list-alumni';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }

    public function Invalid()
    {
        $page = $this->uri->segment(3);

        $limit = 50;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->M_Alumni->FailedAll();
        $set_config = array(
            'base_url' => site_url() . 'Alumni/invalid',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);

        $this->pagination->initialize($config);
        $data['alumni'] = $this->M_Alumni->FailedData($limit, $offset);
        $upt =  $this->M_Alumni->FailedData($limit, $offset);
        foreach ($upt->result() as $key => $value) {
            $msg = '*DPD IKA Undip DKI Jakarta Peduli VAKSINASI Usia 12 Tahun Keatas*--Anda masih bisa mengikuti vaksinasi sesuai jadwal semula saat pendaftaran, silahkan datang pada:--Hari, tanggal : 11 %2D 12 Juli 2021-Pukul  : 11.00 %2D 17.00-Tempat : Kuningan City Lt. 2 Unit 3 %2D 6 -Jl. Prof. Dr. Satrio Kav. 18 -Jakarta Selatan--Terima kasih';
            if ($value->status == 0) {
                if ($key % 2 == 1) {
                    customerService1($value->no_hp, $msg);
                } else {
                    customerService2($value->no_hp, $msg);
                }
            }
            $this->M_Alumni->updateFailed($value->id, ['status' => 1]);
        }
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'list-alumni-failed';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }
}

/* End of file Alumni.php */
