<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Vaksin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('upload');
        
        $this->load->model(['Kategori', 'Anggota', 'Kabkota']);
    }
    public function index()
    {
        
        $page = $this->uri->segment(3);

        $limit = 100;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->Anggota->get_all();
        $set_config = array(
            'base_url' => site_url() . 'Vaksin/index',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);
       $uptUsia = $this->Anggota->getdata($limit, $offset);
        foreach ($uptUsia->result() as $key => $value) {
            if ($value->kodeKategori == '' && $value->jenisPekerjaan != '') {
                $getKategori = $this->db->query('select * from kategori where instansi like "%' . $value->jenisPekerjaan . '%"');
                if ($getKategori->num_rows() > 0) {
                    $getRow = $getKategori->row();
                    $kodeKategori = $getRow->idKategori;
                } else {
                    $kodeKategori = 99;
                }
            } else {
                $kodeKategori = 99;
            }

            $this->Anggota->update($value->id, ['kodeKategori' => $kodeKategori]);
        }
        $this->pagination->initialize($config);
        $data['anggota'] = $this->Anggota->getdata($limit, $offset);
        $data['allData'] = $this->Anggota->get_all();
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'vaksin';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }

    public function send($id)
    {
        $getData = $this->Anggota->get_condition(['id' => $id])->row();
        $this->session->set_flashdata('show', '1');
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('title', 'Perhatian!');
        $this->session->set_flashdata('message', 'Pendaftaran Vaksin Berhasil Dilakukan');
        $hari = getHari(date('D', strtotime($getData->tglVaksin)));
        $bulan = getBulan(date('m', strtotime($getData->tglVaksin)));
        $this->Anggota->update($id, ['sent' => 0]);
        $msg = '*DPD IKA Undip DKI Jakarta Peduli VAKSINASI Usia 18 Tahun Keatas* -- Hallo *' . $getData->nama . '*--Selamat Pendaftaran Anda berhasil, berikut rincian jadwal vaksin anda.--Hari, tanggal : ' . $hari . ', ' . date('d', strtotime($getData->tglVaksin)) . ' ' . $bulan . ' 2021-Pukul  : ' . str_replace('-', '%2D', $getData->detailJam)  . ' -Tempat : Kuningan City Lt. 2 Unit 3%2D6-Jl. Prof. Dr. Satrio Kav. 18 -Jakarta Selatan';
        if ($id % 2 == 1) {
            customerService1($getData->noHp, $msg);
        } else {
            customerService2($getData->noHp, $msg);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function detail()
    {
        $getData = $this->db->get_where('anggota', ['id' => $_POST['id']])->row();
        echo json_encode($getData);
    }

    public function update()
    {
        
         $tanggal_lahir = date_create(set_value(tanggalLahir));
            // tanggal hari ini
            $today = new DateTime('today');
            $diff  = date_diff($tanggal_lahir, $today);
            
        $data = [
            'nama' => set_value('nama'),
            'namaAlumni' => set_value('namaAlumni'),
            'tglVaksin' => set_value('tglVaksin'),
            'jam' => set_value('jam'),
            'nik' => set_value('nik'),
            'status' => set_value('status'),
            'tanggalLahir' => date('Y-m-d', strtotime(set_value('tanggalLahir'))),
            'usia' => $diff->y,
            'sent' => 1,
            'noHp' => set_value('noHp')
        ];
        $this->Anggota->update($this->input->post('id'), $data);
        $this->session->set_flashdata('show', '1');
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('title', 'Perhatian!');
        $this->session->set_flashdata('message', 'Data Berhasil Diperbaharui');
        redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function search(){
         $page = $this->uri->segment(3);

        $limit = 50;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->Anggota->getLike($_GET['nama']);
        $set_config = array(
            'base_url' => site_url() . 'Vaksin/index',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);
        
        $this->pagination->initialize($config);
        $data['anggota'] = $this->Anggota->getLikeLimit($_GET['nama'],$limit, $offset);
        $data['allData'] = $this->Anggota->get_all();
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'vaksin';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }
    
    public function byTanggal($tanggal = ''){
        if($tanggal == '' || strlen($tanggal) < 2){
            $condition = ['tglVaksin' => '2021-07-10'];
            $param = '2021-07-10';
        }else{
            $condition = ['tglVaksin' => $tanggal];
            $param = $tanggal;
        }
        $page = $this->uri->segment(4);

        $limit = 50;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->Anggota->get_condition($condition);
            
        $set_config = array(
            'base_url' => site_url() . 'Vaksin/byTanggal/'.$param,
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);
       
        $this->pagination->initialize($config);
        $data['anggota'] = $this->Anggota->getdataCondtion($condition, $limit, $offset);
        $data['allData'] = $this->Anggota->get_condition($condition);
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'vaksin-by-tanggal';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }
    
     public function byUsia()
    {
        $page = $this->uri->segment(3);

        $limit = 50;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->Anggota->get_all();
        $set_config = array(
            'base_url' => site_url() . 'Vaksin/byUsia',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);

        $this->pagination->initialize($config);
        $data['anggota'] = $this->Anggota->getUsia($limit, $offset);
        $data['allData'] = $this->Anggota->get_all();
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'vaksin-usia';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }
    
    public function upload()
    {
        $data['pagetitle'] = 'Upload Data Vaksin';
        $data['pages'] = 'vaksin-upload';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }

    public function hapus()
    {
        $this->Anggota->delete($this->input->post('id'));
        $this->session->set_flashdata('show', '1');
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('title', 'Perhatian!');
        $this->session->set_flashdata('message', 'Data Berhasil Dihapus dari database');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function downloadExcel()
    {
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=export" . time() . ".xls");
        $data['anggota'] = $this->Anggota->get_all();

        $this->load->view('backend/pages/export-peserta', $data);
    }
}

/* End of file Vaksin.php */
