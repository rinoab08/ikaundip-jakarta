<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('upload');
    }
    public function index()
    {
        $data['pagetitle'] = 'Login';
        $data['pages'] = 'login';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }

    public function auth()
    {
        $this->form_validation->set_rules('username', 'username', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[4]');


        if ($this->form_validation->run() == TRUE) {
            $data = [
                'username' => set_value('username'),
                'password' => sha1(set_value('password'))
            ];
            $getUser = $this->db->get_where('admin', $data);

            if ($getUser->num_rows() == 1) {
                foreach ($getUser->result_array() as $key => $value) {
                    $sess['fullname'] = $value['fullname'];
                    $sess['hakAkses'] = $value['hakAkses'];
                    $sess['logged_in'] = 'logged_in';
                }
                $this->session->set_userdata($sess);
                redirect('alumni', 'refresh');
            } else {
                $this->session->set_flashdata('show', '1');
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('title', 'Perhatian!');
                $this->session->set_flashdata('message', 'Username dan Password yang anda masukkan salah');
                $data['pagetitle'] = 'Login';
                $data['pages'] = 'login';
                $data['nav_active'] = 'transaksi';
                $data['header'] = 'Data transaksi';
                $this->load->view('backend/index', $data);
            }
        } else {
            $this->session->set_flashdata('show', '1');
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('title', 'Perhatian!');
            $this->session->set_flashdata('message', 'Username dan Password harus diisi');
            $data['pagetitle'] = 'Login';
            $data['pages'] = 'login';
            $data['nav_active'] = 'transaksi';
            $data['header'] = 'Data transaksi';
            $this->load->view('backend/index', $data);
        }
    }

    public function destroy()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('fullname');
        $this->session->unset_userdata('hakAkses');
        $this->session->sess_destroy();
        $this->session->set_flashdata('show', '1');
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('title', 'Perhatian!');
        $this->session->set_flashdata('message', 'Anda Berhasil Logout');
        redirect('login');
    }

    public function getSession()
    {

        echo "<pre>";
        print_r($this->session->userdata());
        echo "</pre>";
    }
}

/* End of file Login.php */
