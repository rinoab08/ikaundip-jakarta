<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Peserta extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('upload');
        $this->load->model(['M_Peserta']);
    }

    public function index()
    {
        $page = $this->uri->segment(3);

        $limit = 50;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->M_Peserta->get_all();
        $set_config = array(
            'base_url' => site_url() . 'Peserta/index',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);

        $this->pagination->initialize($config);

        $data['peserta'] = $this->M_Peserta->getdata($limit, $offset);
        $data['allData'] = $this->M_Peserta->get_all();
        $data['register'] = $this->M_Peserta->get_condition_group(['status' => 1]);
        $data['no_empty'] = $this->M_Peserta->get_condition_group(['no_hp' => '-']);
        $data['register_upt'] = $this->M_Peserta->get_condition_group(['status' => 2]);
        $data['ots'] = $this->M_Peserta->get_condition_group(['status' => 0]);
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'vaksin-peserta';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }

    public function detail()
    {
        $getData = $this->db->get_where('peserta_v1_fix', ['idVaksinP' => $_POST['id']])->row();
        echo json_encode($getData);
    }

    public function tidakHadir()
    {
        $page = $this->uri->segment(3);

        $limit = 40;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->M_Peserta->get_condition(['hadir >' => 1]);
        $set_config = array(
            'base_url' => site_url() . 'Peserta/tidakHadir',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);

        $this->pagination->initialize($config);
        // $getPeserta = $this->M_Peserta->getdata($limit, $offset, ['status' => 0]);
        // foreach ($getPeserta->result() as $key => $value) {
        //     $msg = '';
        //     if ($value->no_hp != '-' && $value->blast == 0) {
        //         $msg .= '•DPD IKA UNDIP DKI JAKARTA PEDULI VAKSINASI TAHAP KE 2 USIA 12 TAHUN KEATAS*--Pelaksanaan vaksin tahap kedua di *Kuningan City Mall* akan dimulai mulai besok Sabtu 7 Agustus 2021. --Barangkali karena kesibukan Sdr/Sdri sehingga belum sempat membalas wa dari Panitia, melalui pemberitahuan ini hendaknya Sdr/Sdri dapat segera membuka link di bawah ini untuk memberikan konfirmasi kehadiran: --https://vaksin.ikaundipjakarta.org/welcome/pendaftaran/' . $value->tiket;
        //         $msg .= '--Informasi yang diberikan akan sangat membantu Panitia dalam memberikan pelayanan yang terbaik.--Informasi :-Shabrina 0812 8577 0108-Lioni 0812 1207 4651--Terima kasih.';

        //         if ($value->idVaksinP % 2 == 1) {
        //             customerService1($value->no_hp, $msg);
        //         } else {
        //             customerService2($value->no_hp, $msg);
        //         }
        //         $this->M_Peserta->update($value->idVaksinP, ['blast' => 1]);
        //     }
        // }

        $data['peserta'] = $this->M_Peserta->getdata($limit, $offset, ['hadir >' => 1]);
        $data['allData'] = $this->M_Peserta->get_all();
        $data['register'] = $this->M_Peserta->get_condition_group(['status' => 1]);
        $data['no_empty'] = $this->M_Peserta->get_condition_group(['no_hp' => '-']);
        $data['register_upt'] = $this->M_Peserta->get_condition_group(['status' => 2]);
        $data['ots'] = $this->M_Peserta->get_condition_group(['status' => 0]);
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'vaksin-peserta-tidak-hadir';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }

    public function add()
    {
        $tiket = uniqid();
        $data = [
            'nama' => set_value('nama'),
            'tanggal' => set_value('tanggal'),
            'tiket' => $tiket,
            'blast' => 0,
            'no_hp' => set_value('noHp')
        ];

        $this->db->insert('peserta_v1_fix', $data);

        $msg = '*DPD IKA UNDIP DKI JAKARTA PEDULI VAKSINASI TAHAP KEDUA USIA 12 TAHUN KEATAS* --Sehubungan dengan akan dilaksanakan vaksinasi tahap kedua Sdr/Sdri pada tanggal 7%2D9 Agustus 2021 di *Kuningan City Mall* kami sampaikan beberapa hal sebagai berikut : --1. Jadwal kedatangan sesuai dengan tanggal tercantum di sms yang sudah diterima masing%2Dmasing dari 1199 (pedulilindungi.id).--Sedangkan untuk pengaturan jam kedatangan silakan memilih melalui link berikut ini :-https://vaksin.ikaundipjakarta.org/welcome/pendaftaran/' . $tiket . '-2. Membawa KTP (atau fotocopy KK bagi usia 12 %2D 17 tahun), Kartu Vaksin, Pulpen dan jika diperlukan boleh membawa air minum.-3. Kedatangan hendaknya  menyesuaikan dengan jam pendaftaran, 15 menit sebelumnya agar dapat menjaga ketertibannya.-4. Menjaga kesehatan, tidur yang cukup, mengkonsumsi obat2an wajibnya dan sarapan sebelum vaksin.-5. Saat pengisian Kartu Kendali di lokasi nanti harus mencantumkan nomor hp yang bisa menerima sms karena e-sertifikat vaksin akan dikirim melalui sms dari 1199.--Demi kelancaran vaksinasi tahap kedua Panitia mengajak Sdr/Sdri untuk mengikuti webinar Edukasi Persiapan Vaksinasi tahap kedua yang akan dilaksanakan pada :--*Hari Rabu, tgl 4 Agustus 2021 pukul 19.30 Wib*--Join Zoom Meeting-https://us02web.zoom.us/j/87638939716?pwd=Qk5VWjJnTzNWbXhZUWZXbGJGREtRZz09--Meeting ID : 876 3893 9716-Passcode : vaksin2--Informasi :-Shabrina 0812%2D8577%2D0108-Lioni 0812%2D1207%2D4651--Terima kasih.';
        if ($this->db->insert_id() % 2 == 1) {
            customerService1($_POST['noHp'], $msg);
        } else {
            customerService2($_POST['noHp'], $msg);
        }
        $this->session->set_flashdata('show', '1');
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('title', 'Perhatian!');
        $this->session->set_flashdata('message', 'Data Berhasil Simpan');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function undangan($id)
    {
        $getData = $this->M_Peserta->get_condition(['idVaksinP' => $id])->row();
        $this->session->set_flashdata('show', '1');
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('title', 'Perhatian!');
        $this->session->set_flashdata('message', 'Pendaftaran Vaksin Berhasil Dilakukan');
        $hari = getHari(date('D', strtotime($getData->tanggal)));
        $bulan = getBulan(date('m', strtotime($getData->tanggal)));

        $this->M_Peserta->update($id, ['blast' => 1]);
        $msg = '*DPD IKA UNDIP DKI JAKARTA PEDULI VAKSINASI TAHAP KEDUA USIA 12 TAHUN KEATAS* --Sehubungan dengan akan dilaksanakan vaksinasi tahap kedua Sdr/Sdri pada tanggal 7%2D9 Agustus 2021 di *Kuningan City Mall* kami sampaikan beberapa hal sebagai berikut : --1. Jadwal kedatangan sesuai dengan tanggal tercantum di sms yang sudah diterima masing%2Dmasing dari 1199 (pedulilindungi.id).--Sedangkan untuk pengaturan jam kedatangan silakan memilih melalui link berikut ini :-https://vaksin.ikaundipjakarta.org/welcome/pendaftaran/' . $getData->tiket . '-2. Membawa KTP (atau fotocopy KK bagi usia 12 %2D 17 tahun), Kartu Vaksin, Pulpen dan jika diperlukan boleh membawa air minum.-3. Kedatangan hendaknya  menyesuaikan dengan jam pendaftaran, 15 menit sebelumnya agar dapat menjaga ketertibannya.-4. Menjaga kesehatan, tidur yang cukup, mengkonsumsi obat2an wajibnya dan sarapan sebelum vaksin.-5. Saat pengisian Kartu Kendali di lokasi nanti harus mencantumkan nomor hp yang bisa menerima sms karena e-sertifikat vaksin akan dikirim melalui sms dari 1199.--Demi kelancaran vaksinasi tahap kedua Panitia mengajak Sdr/Sdri untuk mengikuti webinar Edukasi Persiapan Vaksinasi tahap kedua yang akan dilaksanakan pada :--*Hari Rabu, tgl 4 Agustus 2021 pukul 19.30 Wib*--Join Zoom Meeting-https://us02web.zoom.us/j/87638939716?pwd=Qk5VWjJnTzNWbXhZUWZXbGJGREtRZz09--Meeting ID : 876 3893 9716-Passcode : vaksin2--Informasi :-Shabrina 0812%2D8577%2D0108-Lioni 0812%2D1207%2D4651--Terima kasih.';

        if ($id % 2 == 1) {
            customerService1($getData->no_hp, $msg);
        } else {
            customerService2($getData->no_hp, $msg);
        }
        $this->session->set_flashdata('show', '1');
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('title', 'Perhatian!');
        $this->session->set_flashdata('message', 'Data Berhasil Dikirim');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function update()
    {

        $data = [
            'nama' => set_value('nama'),
            'tanggal' => set_value('tglVaksin'),
            'jam' => set_value('jam'),
            'blast' => 0,
            'hadir' => set_value('hadir'),
            'alasan' => set_value('alasan'),
            'status' => 1,
            'no_hp' => set_value('noHp')
        ];
        $this->M_Peserta->update($this->input->post('id'), $data);
        $this->session->set_flashdata('show', '1');
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('title', 'Perhatian!');
        $this->session->set_flashdata('message', 'Data Berhasil Diperbaharui');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function send($id)
    {
        $getData = $this->M_Peserta->get_condition(['idVaksinP' => $id])->row();
        $this->session->set_flashdata('show', '1');
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('title', 'Perhatian!');
        $this->session->set_flashdata('message', 'Pendaftaran Vaksin Berhasil Dilakukan');
        $hari = getHari(date('D', strtotime($getData->tanggal)));
        $bulan = getBulan(date('m', strtotime($getData->tanggal)));
        $getJam = $this->db->get_where('jam', ['idJam' => $getData->jam])->row();
        $this->M_Peserta->update($id, ['blast' => 1]);
        $msg = '*DPD IKA UNDIP DKI JAKARTA PEDULI VAKSINASI TAHAP KEDUA USIA 12 TAHUN KEATAS* --Hallo *' . $getData->nama . '*--Sehubungan ada Pembatasan Quota Vaksin tiap Jam, berikut kami informasikan jadwal terbaru *vaksin kedua* Anda.--Hari, tanggal : *' . $hari . ', ' . date('d', strtotime($getData->tanggal)) . ' ' . $bulan . ' 2021*-Pukul  : *' . str_replace('-', '%2D', $getJam->detailJam)  . '* -Tempat : Kuningan City Lt. 2 Unit 3 %2D 6-Jl. Prof. Dr. Satrio Kav. 18 -Jakarta Selatan--Demi mencegah kerumunan, harap tidak datang terlalu awal maupun terlambat.-Jika datang terlalu awal dari jam seharusnya silakan menunggu di tempat yang telah disediakan Panitia di lantai G depan Coffee Bean, hal ini untuk menghindari penumpukan peserta di lantai 2.';
        if ($id % 2 == 1) {
            customerService1($getData->no_hp, $msg);
        } else {
            customerService2($getData->no_hp, $msg);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function Export()
    {
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=export-peserta-" . time() . ".xls");
        $data['peserta'] = $this->M_Peserta->get_all();
        $this->load->view('backend/pages/peserta-export', $data);
    }

    public function search()
    {
        $page = $this->uri->segment(3);

        $limit = 50;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->M_Peserta->getLike($_GET['nama']);
        $set_config = array(
            'base_url' => site_url() . 'Vaksin/index',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);

        $this->pagination->initialize($config);
        $data['peserta'] = $this->M_Peserta->getLikeLimit($_GET['nama'], $limit, $offset);
        $data['allData'] = $this->M_Peserta->get_all();
        $data['register'] = $this->M_Peserta->get_condition_group(['status' => 1]);
        $data['no_empty'] = $this->M_Peserta->get_condition_group(['no_hp' => '-']);
        $data['register_upt'] = $this->M_Peserta->get_condition_group(['status' => 2]);
        $data['ots'] = $this->M_Peserta->get_condition_group(['status' => 0]);
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'vaksin-peserta';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }

    public function Heregistrasi()
    {
        $page = $this->uri->segment(3);

        $limit = 40;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->M_Peserta->get_condition(['status' => 0]);
        $set_config = array(
            'base_url' => site_url() . 'Peserta/Heregistrasi',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);

        $this->pagination->initialize($config);
        // $getPeserta = $this->M_Peserta->getdata($limit, $offset, ['status' => 0]);
        // foreach ($getPeserta->result() as $key => $value) {
        //     $msg = '';
        //     if ($value->no_hp != '-' && $value->blast == 0) {
        //         $msg .= '•DPD IKA UNDIP DKI JAKARTA PEDULI VAKSINASI TAHAP KE 2 USIA 12 TAHUN KEATAS*--Pelaksanaan vaksin tahap kedua di *Kuningan City Mall* akan dimulai mulai besok Sabtu 7 Agustus 2021. --Barangkali karena kesibukan Sdr/Sdri sehingga belum sempat membalas wa dari Panitia, melalui pemberitahuan ini hendaknya Sdr/Sdri dapat segera membuka link di bawah ini untuk memberikan konfirmasi kehadiran: --https://vaksin.ikaundipjakarta.org/welcome/pendaftaran/' . $value->tiket;
        //         $msg .= '--Informasi yang diberikan akan sangat membantu Panitia dalam memberikan pelayanan yang terbaik.--Informasi :-Shabrina 0812 8577 0108-Lioni 0812 1207 4651--Terima kasih.';

        //         if ($value->idVaksinP % 2 == 1) {
        //             customerService1($value->no_hp, $msg);
        //         } else {
        //             customerService2($value->no_hp, $msg);
        //         }
        //         $this->M_Peserta->update($value->idVaksinP, ['blast' => 1]);
        //     }
        // }

        $data['peserta'] = $this->M_Peserta->getdata($limit, $offset, ['status' => 0]);
        $data['allData'] = $this->M_Peserta->get_all();
        $data['register'] = $this->M_Peserta->get_condition_group(['status' => 1]);
        $data['no_empty'] = $this->M_Peserta->get_condition_group(['no_hp' => '-']);
        $data['register_upt'] = $this->M_Peserta->get_condition_group(['status' => 2]);
        $data['ots'] = $this->M_Peserta->get_condition_group(['status' => 0]);
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'vaksin-peserta';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }

    public function jateng()
    {
        $page = $this->uri->segment(3);

        $limit = 20;
        if (!$page) :
            $offset = 0;
        else :
            $offset = $page;
        endif;
        $table = $this->M_Peserta->getDataJatengAll();
        $set_config = array(
            'base_url' => site_url() . 'Peserta/jateng',
            'table'        => $table->num_rows(),
            'limit'        => $limit
        );
        $config = pagination_helper($set_config);

        $this->pagination->initialize($config);
        $getPeserta = $this->M_Peserta->getDataJateng($limit, $offset);
        foreach ($getPeserta->result() as $key => $value) {
            $msg = '';
            if ($value->blast == 0) {
                if (date('d-m-Y', strtotime($value->tanggal)) == '14-08-2021') {
                    $hari = 'Sabtu, 14 Agustus 2021';
                } else {
                    $hari = 'Minggu, 15 Agustus 2021';
                }
                $msg .= '*VAKSINASI PERTAMA IKA UNDIP JATENG USIA 18 TAHUN KEATAS*--Halo *' . $value->nama . '*--Tanggal 12 %2D 13 Agustus 2021 Anda sudah terdaftar di google form Vaksinasi IKA Undip Jateng. --Jika blm vaksinasi atau belum mendapat undangan dari Kemenkes RI (loket.com), silakan bisa ikut vaksinasi di hari terakhir langsung datang saja pada,--Hari, tanggal : *' . $hari . '*-Pukul : *11.00%2D14.00* WIB.-Tempat : Auditorium Undip Pleburan.';
                $msg .= '--Kepada Petugas di gate pemeriksaan bilang *sudah daftar di google form IKA Undip tetapi belum mendapatkan undangan dari Kemenkes RI.*. Oleh Petugas akan diarahkan masuk karena datanya sudah ada di Petugas Registrasi yang di dalam Auditorium.--Mohon diabaikan pemberitahuan ini jika sdh pernah menerima undangan vaksinasi di Undip Pleburan.--Informasi : -1. Catur 0896%2D2117%2D2454-2. Arkham 0812%2D2881%2D9994-3. Iwan 0855%2D4050%2D2161-4. Wawan 0817%2D993%2D7776--Terima kasih,-DPD IKA Undip Jateng';
                if ($value->no_hp != 'N') {
                    $noHp = $value->no_hp;
                    customerService2($noHp, $msg);
                    // if ($value->idVaksinP % 2 == 0) {
                    //     customerService1($noHp, $msg);
                    // } else {
                        
                    // }
                }

                $this->M_Peserta->update($value->idVaksinP, ['blast' => 1]);
            }
        }

        $data['peserta'] = $this->M_Peserta->getDataJateng($limit, $offset);
        $data['allData'] = $this->M_Peserta->get_all();
        $data['register'] = $this->M_Peserta->get_condition_group(['status' => 1]);
        $data['no_empty'] = $this->M_Peserta->get_condition_group(['no_hp' => '-']);
        $data['register_upt'] = $this->M_Peserta->get_condition_group(['status' => 2]);
        $data['ots'] = $this->M_Peserta->get_condition_group(['status' => 0]);
        $data["paginator"]    = $this->pagination->create_links();
        $data['pagetitle'] = 'Transaksi';
        $data['pages'] = 'vaksin-peserta';
        $data['nav_active'] = 'transaksi';
        $data['header'] = 'Data transaksi';
        $this->load->view('backend/index', $data);
    }
}

/* End of file Peserta.php */
