<?php
function has_fill_noHP()
{
    $CI = &get_instance();
    if (isset($_SESSION['id_user']) && $_SESSION['id_user'] != '') {
        $CI->db->where('id_user', $_SESSION['id_user']);
        $CI->db->where('no_telp !=', '');
        $count = $CI->db->get('user')->num_rows();

        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

function get_color($limit)
{
    $arr_color = array('#ffc40f', '#c40b0b', '#1ca', '#b973e0', '#1cafff', '#00b139', '#d0c0c6', '#50af9f', '#cc4b00', 'pink', '#6d150d', '#a79b82', '#F0E68C', '#ae8b00', '#da8700', '#617400', '#f0c843', '#449686', '#00FF00', '#008000', '#FF6347', '#B22222', '#F08080', '#808000', '#008B8B', '#7FFFD4', '#7B68EE', '#FF00FF', '#9932CC', '#DB7093', '#808080', '#D2B48C', '#ffc40f', '#c40b0b', '#1ca', '#b973e0', '#1cafff', '#00b139', '#d0c0c6', '#50af9f', '#cc4b00', 'pink', '#6d150d', '#a79b82', '#F0E68C', '#ae8b00', '#da8700', '#617400', '#f0c843', '#449686', '#00FF00', '#008000', '#FF6347', '#B22222', '#F08080', '#808000', '#008B8B', '#7FFFD4', '#7B68EE', '#FF00FF', '#9932CC', '#DB7093', '#808080', '#D2B48C', '#ffc40f', '#c40b0b', '#1ca', '#b973e0', '#1cafff', '#00b139', '#d0c0c6', '#50af9f', '#cc4b00', 'pink', '#6d150d', '#a79b82', '#F0E68C', '#ae8b00', '#da8700', '#617400', '#f0c843', '#449686', '#00FF00', '#008000', '#FF6347', '#B22222', '#F08080', '#808000', '#008B8B', '#7FFFD4', '#7B68EE', '#FF00FF', '#9932CC', '#DB7093', '#808080', '#D2B48C');
    $new_arr = array_slice($arr_color, 0, $limit);
    return json_encode($new_arr);
}


function backend($variable = '')
{
    return site_url('administrator/' . $variable);
}

function pathUrl($variable = '')
{
    return site_url($variable);
}

function asset($variable)
{
    return base_url('public/' . $variable);
}

function unggah()
{
    return base_url('uploads/');
}


function fl_post($index)
{
    $CI = &get_instance();
    if ($CI->session->flashdata('post') != '') {
        $post = $CI->session->flashdata('post');
        if (isset($post[$index])) {
            return $post[$index];
        } else {
            return "";
        }
    } else {
        return "";
    }
}

function callAPI($method, $url)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: 8e9468a1853ceeffc9596c85a286fa25"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function check_login()
{
    $CI = &get_instance();
    if (empty($CI->session->userdata('fullname'))) {
        redirect('login');
    }
}

function pagination_helper($paginator)
{
    $config['base_url']         = $paginator['base_url'];

    $config['total_rows']         = $paginator['table'];
    $config['per_page']         = $paginator['limit'];

    $config['full_tag_open']     = '<nav class="pl-3 pr-3"><ul class="pagination justify-content-center">';
    $config['full_tag_close']     = '</ul></nav>';

    $config['next_link']        = 'Next &rarr;';
    $config['prev_link']        = '&larr; Prev';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']     = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']     = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']     = 'Next</span></li>';
    $config['prev_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']     = 'Previous</span></li>';
    $config['first_link']        = '&larr; First';
    $config['last_link']        = 'Last &rarr;';
    $config['first_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']     = '</span></li>';

    return $config;
}

function email_sender()
{
    $config['protocol']     = "smtp";
    $config['smtp_host']     = "ssl://mail.menikahdenganku.com";
    $config['smtp_port']     = 465;
    $config['smtp_user']     = "info@menikahdenganku.com";
    $config['smtp_pass']     = "=@gd@}2i~P;r";
    $config['charset']         = "utf-8";
    $config['mailtype']     = "html";
    $config['newline']         = "\r\n";
    $config['wordwrap']        = TRUE;
    $config['validation']     = TRUE;

    return $config;
}

function helper_upload($data)
{
    $config['upload_path']         = './' . $data['direktori'];
    $config['file_name']         = $data['fileName'];
    $config['allowed_types']    = 'png|jpg|jpeg|bmp|JPG';
    $config['overwrite']          = TRUE;
    $config['max_size']          = 50000;

    return $config;
}

function slug_generate($title)
{
    $search = array(' ', '!', '@', '#', '$', '^', '&', '%', '*', '(', ')', '+', '=', '[', ']', '/', '{', '}', '|', ':', '<', '>', '?', ',', '.', '"', '-');
    $replace = array('-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '', '-', '-', '-', '-');
    $slug_title = str_replace($search, $replace, strtolower($title));

    return $slug_title;
}

function resize_image($file, $w, $h, $crop = false)
{
    list($width, $height) = getimagesize($file);
    $r = $width / $height;
    if ($crop) {
        if ($width > $height) {
            $width = ceil($width - ($width * abs($r - $w / $h)));
        } else {
            $height = ceil($height - ($height * abs($r - $w / $h)));
        }
        $newwidth = $w;
        $newheight = $h;
    } else {
        if ($w / $h > $r) {
            $newwidth = $h * $r;
            $newheight = $h;
        } else {
            $newheight = $w / $r;
            $newwidth = $w;
        }
    }

    //Get file extension
    $exploding = explode(".", $file);
    $ext = end($exploding);

    switch ($ext) {
        case "png":
            $src = imagecreatefrompng($file);
            break;
        case "jpeg":
        case "jpg":
            $src = imagecreatefromjpeg($file);
            break;
        case "gif":
            $src = imagecreatefromgif($file);
            break;
        default:
            $src = imagecreatefromjpeg($file);
            break;
    }

    $dst = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    return $dst;
}

function resizeImage_2($sourceImage, $targetImage, $maxWidth, $maxHeight, $quality = 80)
{
    // Obtain image from given source file.
    if (!$image = @imagecreatefromjpeg($sourceImage)) {
        return false;
    }

    // Get dimensions of source image.
    list($origWidth, $origHeight) = getimagesize($sourceImage);

    if ($maxWidth == 0) {
        $maxWidth  = $origWidth;
    }

    if ($maxHeight == 0) {
        $maxHeight = $origHeight;
    }

    // Calculate ratio of desired maximum sizes and original sizes.
    $widthRatio = $maxWidth / $origWidth;
    $heightRatio = $maxHeight / $origHeight;

    // Ratio used for calculating new image dimensions.
    $ratio = min($widthRatio, $heightRatio);

    // Calculate new image dimensions.
    $newWidth  = (int)$origWidth  * $ratio;
    $newHeight = (int)$origHeight * $ratio;

    // Create final image with new dimensions.
    $newImage = imagecreatetruecolor($newWidth, $newHeight);
    imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
    imagejpeg($newImage, $targetImage, $quality);

    // Free up the memory.
    imagedestroy($image);
    imagedestroy($newImage);

    return true;
}
function getApi($parameter)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $parameter['url'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $parameter['postFields'],
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: 8e9468a1853ceeffc9596c85a286fa25"
        ),
    ));
}

function getBulan($bulan)
{
    switch ($bulan) {
        case '01':
            $nama_bulan = 'Januari';
            break;
        case '02':
            $nama_bulan = 'Februari';
            break;
        case '03':
            $nama_bulan = 'Maret';
            break;
        case '04':
            $nama_bulan = 'April';
            break;
        case '05':
            $nama_bulan = 'Mei';
            break;
        case '06':
            $nama_bulan = 'Juni';
            break;
        case '07':
            $nama_bulan = 'Juli';
            break;
        case '08':
            $nama_bulan = 'Agustus';
            break;
        case '09':
            $nama_bulan = 'September';
            break;
        case '10':
            $nama_bulan = 'Oktober';
            break;
        case '11':
            $nama_bulan = 'November';
            break;

        default:
            $nama_bulan = 'Desember';
            break;
    }
    return $nama_bulan;
}

function getHari($parameter)
{
    if ($parameter == 'Mon') {
        $hari = 'Senin';
    } else if ($parameter == 'Tue') {
        $hari = 'Selasa';
    } else if ($parameter == 'Wed') {
        $hari = 'Rabu';
    } else if ($parameter == 'Thu') {
        $hari = 'Kamis';
    } else if ($parameter == 'Fri') {
        $hari = 'Jumat';
    } else if ($parameter == 'Sat') {
        $hari = 'Sabtu';
    } else {
        $hari = 'Minggu';
    }
    return $hari;
}

function StatusText($parameter)
{
    switch ($parameter) {
        case 0:
            $progress = 'Menunggu';
            break;
        case 1:
            $progress = 'Diproses';
            break;
        case 2:
            $progress = 'Dikirim';
            break;
        case 3:
            $progress = 'Diterima';
            break;
        case 4:
            $progress = 'Selesai';
            break;
        default:
            $progress = 'Kadaluarsa';
            break;
    }

    return $progress;
}



function StatusBadge($parameter)
{
    switch ($parameter) {
        case 0:
            $progress = 'badge-warning';
            break;
        case 1:
            $progress = 'badge-info';
            break;
        case 2:
            $progress = 'badge-primary';
            break;
        case 3:
            $progress = 'badge-success';
            break;
        case 4:
            $progress = 'badge-dark';
            break;
        default:
            $progress = 'badge-danger';
            break;
    }

    return $progress;
}

function StatusNum($parameter)
{
    switch ($parameter) {
        case 0:
            $progress = 20;
            break;
        case 1:
            $progress = 40;
            break;
        case 2:
            $progress = 60;
            break;
        case 3:
            $progress = 80;
            break;
        default:
            $progress = 100;
            break;
    }

    return $progress;
}

function customerService1($dest, $msg)
{
    $my_apikey = "0SR8VB1EPDARIH23JA2P";
    // $my_apikey = "Z8N4UXY3AVQ9XZ3ST6TI";
    $msg = str_replace(" ", "%20", $msg);
    $msg = str_replace("-", "%0A", $msg);
    $api_url = "http://panel.apiwha.com/send_message.php";
    $api_url .= "?apikey=" . urlencode($my_apikey);
    $api_url .= "&number=" . urlencode($dest);
    $api_url .= "&text=" . $msg;

    $my_result_object = json_decode(file_get_contents($api_url, false));
    echo "<br>Result: " . $my_result_object->success;
    echo "<br>Description: " . $my_result_object->description;
    echo "<br>Code: " . $my_result_object->result_code;
}
function customerService2($dest, $msg)
{
    $my_apikey = "FOJPSF2NP9YCZ7L3O4R5";
    $msg = str_replace(" ", "%20", $msg);
    $msg = str_replace("-", "%0A", $msg);
    $api_url = "http://panel.apiwha.com/send_message.php";
    $api_url .= "?apikey=" . urlencode($my_apikey);
    $api_url .= "&number=" . urlencode($dest);
    $api_url .= "&text=" . $msg;

    $my_result_object = json_decode(file_get_contents($api_url, false));
    echo "<br>Result: " . $my_result_object->success;
    echo "<br>Description: " . $my_result_object->description;
    echo "<br>Code: " . $my_result_object->result_code;
}
