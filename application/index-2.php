<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pendaftaran Vaksin DPD IKA Undip DKI Jakarta</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900|Staatliches" rel="stylesheet">
    <link rel="stylesheet" href="<?= asset('vendors/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/OwlCarousel/css/owl.carousel.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/datepicker/css/bootstrap-datepicker.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/OwlCarousel/css/owl.theme.default.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/easyautocomplate/easy-autocomplete.min.css') ?>">
    <link rel="stylesheet" href="<?= asset('vendors/easyautocomplate/easy-autocomplete.themes.min.css') ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="<?= asset('css/style.css') ?>">
</head>

<body>
    <header>
        <div class="top-nav">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs text-left">
                        <ul class="list-inline list-inline-top mb-0 py-2 font-size-13-px">
                            <li class="d-inline mr-4">
                                <a href="">
                                    <span class="text-white-50">info@ikaundipjakarta.org</span>
                                </a>
                            </li>
                            <li class="d-inline mr-5">
                                <a href="">
                                    <span class="text-white-50">0815-1177-7999</span>
                                </a>
                            </li>
                            <li class="d-inline mr-3">
                                <a href="">
                                    <span class="text-white-50"><i class="fa fa-facebook" aria-hidden="true"></i></span>
                                </a>
                            </li>
                            <li class="d-inline mr-3">
                                <a href="">
                                    <span class="text-white-50"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
                                </a>
                            </li>
                            <li class="d-inline mr-3">
                                <a href="">
                                    <span class="text-white-50"><i class="fa fa-instagram" aria-hidden="true"></i></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 text-right pull-right ">
                        <ul class="list-inline list-inline-top mb-0 py-2 font-size-13-px text-right position-absolute right-0">
                            <li class="d-inline">
                                <form class="form-inline custome-form">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Pencarian" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-outline-secondary" type="button" id="button-addon1"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light" id="nav-custome">
            <a class="navbar-brand" href="#">
                <img src="<?= asset('image/logo-header-1.png') ?>">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item text-center">
                        <a class="nav-link" href="#">NEWS</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="#">DIREKTORI ALUMNI</a>
                    </li>
                </ul>
                <ul class="navbar-nav mr-0">
                    <li class="nav-item text-center">
                        <a class="nav-link" href="#">info</a>
                    </li>
                    <li class="nav-item text-center">
                        <a class="nav-link" href="#">tentang kami</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="content pb-5">
        <div class="container">
            <?php $getPage = $this->db->get_where('testing', ['idTeast' => 1])->row(); ?>
            <div class="carousel slide" data-ride="carousel" id="form-detail">
                <div class="carousel-inner">

                    <div class="box-progress mb-4 hidden-xs d-none">
                        <div class="progress">
                            <div class="circle progres_1 done">
                                <span class="label">1</span>
                                <span class="title">Waktu</span>
                            </div>
                            <span class="bar progres_2" data-progres="2"></span>
                            <div class="circle progres_2" data-progres="2">
                                <span class="label">2</span>
                                <span class="title">Identitas</span>
                            </div>
                            <span class="bar progres_3" data-progres="3"></span>
                            <div class="circle progres_3" data-progres="3">
                                <span class="label">3</span>
                                <span class="title">Riwayat Kesehatan</span>
                            </div>
                            <span class="bar progres_4" data-progres="4"></span>
                            <div class="circle progres_4" data-progres="4">
                                <span class="label">4</span>
                                <span class="title">Persetujuan</span>
                            </div>
                        </div>

                    </div>
                    <form action="<?= site_url('welcome/updateData') ?>" method="POST" <?= ($getPage->status == 1) ? 'id="formId"' : '' ?>>
                        <div class="row">
                            <div class="col-md-8 col-12 mx-auto">
                                <div class="px-4">
                                    <div class="alert alert-primary mb-3" role="alert">
                                        <strong>Kontak Person</strong><br>
                                        Apabila 15 menit setelah mengirim form Anda tidak mendapatkan Pesan Konfirmasi di WhatsApp, silahkan menghubungi Call Center di 62812-8577-0108 & 62812-1207-4651
                                    </div>
                                </div>
                                <div class="row">

                                    <?php if ($getPage->status == 1) { ?>

                                        <div class="col-sm-12 mx-auto">
                                            <div class="alert alert-success" role="alert">
                                                <h4 class="alert-heading">Perhatian</h4>
                                                <p>Pendaftaran Vaksin IKA Undip DPD DKI Jakarta Telah Ditutup</p>

                                            </div>
                                        </div>

                                    <?php } else { ?>

                                        <div class="col-12 d-none">
                                            <div class="form-group">
                                                <div class="alert alert-primary" role="alert">
                                                    <strong>Pilih Jadwal</strong><br>
                                                    Jadwal Vaksin yang Tersedia diantaranya
                                                    <ul>

                                                        <?php
                                                        $arr_hari = ["2021-08-07", "2021-08-08", "2021-08-09"];
                                                        foreach ($arr_hari as $key => $value) {
                                                            $get_jml = $this->db->get_where('peserta_v1_fix', ['tanggal' => $value]);
                                                            if ($get_jml->num_rows() < 800) {

                                                        ?>
                                                                <li> <?= date('d-m-Y', strtotime($value)) ?> :
                                                                    <?php
                                                                    foreach ($jam as $key_j => $value_j) {
                                                                        $get_jml = $this->db->get_where('peserta_v1_fix', ['tanggal' => $value, 'jam' => $value_j->idJam]);
                                                                        if ($get_jml->num_rows() < 125) {
                                                                            echo $value_j->detailJam . '; ';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </li>
                                                        <?php }
                                                        } ?>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php foreach ($peserta->result_array() as $key => $value_p) { ?>
                                        <input type="hidden" name="idVaksinP" value="<?= $value_p['idVaksinP'] ?>">
                                        <div class="col-sm-6 col-12 mt-3">
                                            <div class="form-group">
                                                <label for="forNama">Nama Penerima Vaksi</label>
                                                <input type="text" class="form-control" value="<?= $value_p['nama'] ?>" disabled>
                                                <input type="hidden" name="nama" value="<?= $value_p['nama'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12 mt-3">
                                            <div class="form-group">
                                                <label for="forNama">No WhatsApp</label>
                                                <input type="text" class="form-control" value="<?= $value_p['no_hp'] ?>" disabled>
                                                <input type="hidden" name="noHp" value="<?= $value_p['no_hp'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Tanggal</label>
                                                <input type="hidden" name="tglVaksin" value="<?= date('Y-m-d', strtotime($value_p['tanggal'])) ?>">
                                                <select id="tglVaksin" class="form-control" readonly>
                                                    <option value="0">Pilih Tanggal Vaksin</option>
                                                    <option value="2021-08-07" <?= (date('Y-m-d', strtotime($value_p['tanggal'])) == "2021-08-07") ? 'selected' : '' ?>>Sabtu, 07 Agustus 2021</option>
                                                    <option value="2021-08-08" <?= (date('Y-m-d', strtotime($value_p['tanggal'])) == "2021-08-08") ? 'selected' : '' ?>>Minggu, 08 Agustus 2021</option>
                                                    <option value="2021-08-09" <?= (date('Y-m-d', strtotime($value_p['tanggal'])) == "2021-08-08") ? 'selected' : '' ?>>Senin, 09 Agustus 2021</option>
                                                </select>
                                                <span class="text-danger font-sm"><?= form_error('tglVaksin') ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group">
                                                <label for="jam">Jam</label>
                                                <select name="jam" id="jam" class="form-control">

                                                    <?php foreach ($jam as $key => $value) { ?>
                                                        <option value="<?= $value->idJam ?>" <?= (set_value('jam') == $value->idJam) ? 'selected' : '' ?>><?= $value->detailJam ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-danger font-sm"><?= form_error('jam') ?></span>
                                            </div>
                                        </div>
                                        <div class="col-12 d-none" id="note">
                                            <div class="form-group">
                                                <div class="alert alert-danger" role="alert">
                                                    Quota pada hari dan jam tersebut sudah terpenuhi, silahkan pilih lainnya.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 text-right  d-none mt-4" id="tmb-next">
                                            <button class="btn btn-success" id="btnRegister" type="submit"><i class="fa fa-paper-plane mr-2" aria-hidden="true"></i>Simpan</button>
                                        </div>
                                    <?php } ?>
                                </div>



                            </div>
                        </div>

                    </form>
                </div>


            </div>


        </div>
    </div>
    <footer class="py-3 text-center bg-ika-undip position-relative bottom-0 w-100 text-white mt-5">
        <div class="contrainer">
            <p class="mb-0"> @<?= date('Y') ?> | IKA Universitas Diponegoro DPD DKI Jakarta</p>
        </div>
    </footer>
    <script>
        var url = '<?= site_url('welcome') ?>';
    </script>
    <script src="<?= asset('js/jquery-3.4.1.min.js') ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="<?= asset('vendors/OwlCarousel/js/owl.carousel.js') ?>"></script>
    <script src="<?= asset('vendors/datepicker/js/bootstrap-datepicker.min.js') ?>"></script>
    <script src="<?= asset('vendors/easyautocomplate/jquery.easy-autocomplete.min.js') ?>"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="<?= asset('js/app.js?v=6') ?>"></script>
    <script>
        $(document).ready(function() {


            <?php if ($this->session->flashdata('show') == 1) { ?>

                Swal.fire({
                    title: '<?php echo $this->session->flashdata('title'); ?>',
                    text: '<?php echo $this->session->flashdata('message'); ?>',
                    icon: '<?php echo $this->session->flashdata('type'); ?>',
                    confirmButtonText: 'Tutup'
                })
            <?php } ?>
        })
    </script>
</body>

</html>