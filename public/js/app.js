$(document).ready(function () {
    var win_height = $(window).height();
    var nav_height = $("header").outerHeight();
    var footer_height = $("footer").outerHeight();
    var content_height = win_height - (nav_height + footer_height);
    // $("body").css({"padding-top":nav_height+"px"});
    $(".content .container").css({ 'min-height': content_height + "px" });
    $('.use-datepicker').datepicker({
        format: 'dd-mm-yyyy',
        endDate: new Date('2009-07-10')
    });
    $('.use-select2').select2();
    var form = document.getElementById("formId");
    var allElements = form.elements;
    for (var i = 0, l = allElements.length; i < l; ++i) {
        // allElements[i].readOnly = true;
        allElements[i].disabled = true;
    }

})
$("#form-detail").carousel({
    interval: false,
});
$(".btn-next").click(function (e) {
    e.preventDefault();
    $("#form-detail").carousel("next")
    $('.progres_' + $(this).attr('data-id')).addClass('done');
});

$(".btn-prev").click(function (e) {
    e.preventDefault();
    $("#form-detail").carousel("prev")
    var lastDataId = parseInt($(this).attr('data-id')) + 1;
    $('.progres_' + lastDataId).removeClass('done');
});
$('#hadir').change(function(){
    if ($(this).val() == 1) {
        $('#jam-box').removeClass('d-none');
        $('#alasan-box').addClass('d-none');
    }else{
        $('#alasan-box').removeClass('d-none');
        $('#jam-box').addClass('d-none');
    }
})

$('#sel_alasan').change(function(){
    if ($(this).val() == 8) {
        $('#txt-alasan').removeClass('d-none');
    }else{
        $('#txt-alasan').addClass('d-none');
    }
    $('#tmb-next').removeClass('d-none');
})
$('#tglVaksin').change(function () {
    var tanggal = $(this).val();
    var jam = $('#jam').val();
    var quota = 105;

    $.ajax({
        method: 'POST',
        url: url + '/cekQuota',
        data: {
            tanggal: tanggal,
            jam: jam
        },
        dataType: "json",
        success: function (data) {
            if (data >= quota) {
                $('#note').removeClass('d-none');
                $('#tmb-next').addClass('d-none');
            } else {
                $('#note').addClass('d-none');
                $('#tmb-next').removeClass('d-none');
            }
            console.log(data);
        },
        error: function (XMLHttpRequest) {
            console.log(XMLHttpRequest);
        }
    });
})
$('#jam').change(function () {
    var tanggal = $('#tglVaksin').val();
    var jam = $(this).val();
    var quota = 105;

    $.ajax({
        method: 'POST',
        url: url + '/cekQuota',
        data: {
            tanggal: tanggal,
            jam: jam
        },
        dataType: "json",
        success: function (data) {
            if (data >= quota) {
                $('#note').removeClass('d-none');
                $('#tmb-next').addClass('d-none');
            } else {
                $('#note').addClass('d-none');
                $('#tmb-next').removeClass('d-none');
            }
        },
        error: function (XMLHttpRequest) {
            console.log(XMLHttpRequest);
        }
    });
});

$('#statusPeserta').change(function () {
    if ($(this).val() == 3) {
        $('#namaAlumni').attr('readonly', 'true');
        $('#fakultasAlumni').attr('readonly', 'true');
    } else {
        $('#namaAlumni').removeAttr('readonly');
        $('#fakultasAlumni').removeAttr('readonly');
    }
});

